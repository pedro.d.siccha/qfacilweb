@extends('layouts.app')
@section('titulo')
    ENVIAR
@endsection

@section('contenido')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>ENVIAR</h2>
    
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ Route('inicio') }}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Tiendas</span></li>
                <li><span>Enviar</span></li>
            </ol>
    
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
</section>
@endsection