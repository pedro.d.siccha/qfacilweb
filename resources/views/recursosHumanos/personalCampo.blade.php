@extends('layouts.app')
@section('titulo')
    PERSONAL DE CAMPO
@endsection

@section('contenido')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>PERSONAL DE CAMPO</h2>
    
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ Route('inicio') }}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Área Legal</span></li>
                <li><span>Personal de Campo</span></li>
            </ol>
    
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Personal</h2>
            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-none">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>DNI</th>
                                <th>Número</th>
                                <th>Registro</th>
                                <th>Puntaje</th>
                                <th>Administración</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</section>
@endsection