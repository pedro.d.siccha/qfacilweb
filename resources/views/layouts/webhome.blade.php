<!DOCTYPE html>
<html lang="es"> 
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link rel="shortcut icon" href="img/web/logo.png">
<title> @yield('titulo') QFacil Perú</title>
   <link href="web/web/fonts.googleapis.com/css76f9.css?family=Roboto%3A%2C400%2C700%2C900%7CBarlow%3A800%2C700%2C%2C400%2C900" rel="stylesheet"><meta name='robots' content='max-image-preview:large' />
   <link rel='dns-prefetch' href='web/web/fonts.googleapis.com/index.html' />
   <link rel='dns-prefetch' href='web/web/s.w.org/index.html' />
   <link rel="alternate" type="application/rss+xml" title="QFacil Perú &raquo; Feed" href="web/feed/index.html" />
   <link rel="alternate" type="application/rss+xml" title="QFacil Perú &raquo; Feed de los comentarios" href="web/comments/feed/index.html" />
   <script type="text/javascript">
      window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.qfacilperu.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.7"}};
      !function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
   </script>
	<style type="text/css">
      img.wp-smiley,
      img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
      }
   </style>
	<link rel='stylesheet' id='wp-block-library-css'  href='{{ ('web/wp-includes/css/dist/block-library/style.mine23c.css?ver=5.7') }}' type='text/css' media='all' />
   <link rel='stylesheet' id='wc-block-vendors-style-css'  href='{{("web/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-stylecb20.css?ver=4.4.3")}}' type='text/css' media='all' />
   <link rel='stylesheet' id='wc-block-style-css'  href='{{ ("web/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/stylecb20.css?ver=4.4.3") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='wpc-block-style-css-css'  href='{{ ("web/wp-content/plugins/wp-cafe/core/guten-block/dist/blocks.style.build.css") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='contact-form-7-css'  href='{{ ("web/wp-content/plugins/contact-form-7/includes/css/styles91d5.css?ver=5.4") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='rs-plugin-settings-css'  href='{{ ("web/wp-content/plugins/revslider/public/assets/css/rs6fab5.css?ver=6.3.8") }}' type='text/css' media='all' />
   <style id='rs-plugin-settings-inline-css' type='text/css'>
      #rs-demo-id {}
   </style>
   <link rel='stylesheet' id='woocommerce-layout-css'  href='{{ ("web/wp-content/plugins/woocommerce/assets/css/woocommerce-layout0e7d.css?ver=5.1.0") }}' type='text/css' media='all' />
   <style id='woocommerce-layout-inline-css' type='text/css'>
      .infinite-scroll .woocommerce-pagination {
         display: none;
      }
   </style>
   <link rel='stylesheet' id='woocommerce-smallscreen-css'  href='{{ ("web/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen0e7d.css?ver=5.1.0") }}' type='text/css' media='only screen and (max-width: 768px)' />
   <link rel='stylesheet' id='woocommerce-general-css'  href='{{ ("web/wp-content/plugins/woocommerce/assets/css/woocommerce0e7d.css?ver=5.1.0") }}' type='text/css' media='all' />
   <style id='woocommerce-inline-inline-css' type='text/css'>
      .woocommerce form .form-row .required { visibility: visible; }
   </style>
   <link rel='stylesheet' id='wur_content_css-css'  href='{{("web/wp-content/plugins/wp-ultimate-review/assets/public/css/content-pagee23c.css?ver=5.7")}}' type='text/css' media='all' />
   <link rel='stylesheet' id='dashicons-css'  href='{{ ("web/wp-includes/css/dashicons.mine23c.css?ver=5.7") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='flatpicker-css'  href='{{ ("web/wp-content/plugins/wp-cafe/assets/css/flatpickr.mine7f0.css?ver=1.3.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='jquery-timepicker-css'  href='{{ ("web/wp-content/plugins/wp-cafe/assets/css/jquery.timepicker.mine7f0.css?ver=1.3.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='wpc-icon-css'  href='{{ ("web/wp-content/plugins/wp-cafe/assets/css/wpc-icone7f0.css?ver=1.3.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='wpc-public-css'  href='{{ ("web/wp-content/plugins/wp-cafe/assets/css/wpc-publice7f0.css?ver=1.3.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementskit-parallax-style-css'  href='{{ ("web/wp-content/themes/gloreya/core/parallax/assets/css/style7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-icons-ekiticons-css'  href='{{ ("web/wp-content/plugins/elementskit-lite/modules/controls/assets/css/ekiticonsad76.css?ver=5.9.0") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-icons-css'  href='{{ ("web/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min21f9.css?ver=5.11.0") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-animations-css'  href='{{ ("web/wp-content/plugins/elementor/assets/lib/animations/animations.minaeb9.css?ver=3.1.4") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-frontend-legacy-css'  href='{{ ("web/wp-content/plugins/elementor/assets/css/frontend-legacy.minaeb9.css?ver=3.1.4") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-frontend-css'  href='{{ ("web/wp-content/plugins/elementor/assets/css/frontend.minaeb9.css?ver=3.1.4") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-post-1489-css'  href='{{ ("web/wp-content/uploads/elementor/css/post-14897072.css?ver=1616104051") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementskit-css-widgetarea-control-editor-css'  href='{{ ("web/wp-content/plugins/elementskit-lite/modules/controls/assets/css/widgetarea-editor77e6.css?ver=2.2.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-global-css'  href='{{ ("web/wp-content/uploads/elementor/css/global7072.css?ver=1616104051") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-post-579-css'  href='{{ ("web/wp-content/uploads/elementor/css/post-5799708.css?ver=1616115895") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='gloreya-fonts-css'  href='{{ ("web/web/fonts.googleapis.com/cssc63e.css?family=Barlow%3A300%2C300i%2C400%2C400i%2C500%2C500i%2C600%2C600i%2C700%2C700i%2C800%2C800i%2C900%2C900i%7CRoboto%3A300%2C300i%2C400%2C400i%2C500%2C500i%2C700%2C700i%2C900%2C900i&amp;ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='bootstrap-min-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/bootstrap.min7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='font-awesome-5-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/font-awesome7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='iconfont-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/iconfont7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='magnific-popup-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/magnific-popup7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='owl-carousel-min-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/owl.carousel.min7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='gloreya-woocommerce-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/woocommerce7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='gloreya-gutenberg-custom-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/gutenberg-custom7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='gloreya-style-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/master7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <style id='gloreya-style-inline-css' type='text/css'>

        body{ font-family:"Roboto";font-size:16px; }

        h1{
            font-family:"Barlow";font-weight:800;
        }
        h2{
            font-family:"Barlow";font-weight:700; 
        }
        h3{ 
            font-family:"Barlow";font-weight:700; 
        }

        h4{ 
            font-family:"Barlow";font-weight:700;
        }      

        a, .post-meta span i, .entry-header .entry-title a:hover, .sidebar ul li a:hover{
            color: #e7272d;
        }

        .entry-header .entry-title a,
        .post .entry-header .entry-title a{
            color: #222;
        }
     
        body{
            background-color: #fff;
        }
      
     
        .single-intro-text .count-number, .sticky.post .meta-featured-post,
        .sidebar .widget .widget-title:before, .pagination li.active a, .pagination li:hover a,
        .pagination li.active a:hover, .pagination li:hover a:hover,
        .sidebar .widget.widget_search .input-group-btn,
        .BackTo, .ticket-btn.btn:hover,
        .woocommerce div.product form.cart .button,
        .btn-primary,
        .BackTo,
        .header-book-btn .btn-primary,
        .header .navbar-container .navbar-light .main-menu > li > a:before,
        .header-transparent:before,
        .header-transparent .header-cart .cart-link a sup,
        .header-transparent .navSidebar-button,
        .owl-carousel .owl-dots .owl-dot.active,
        .testimonial-carousel.owl-carousel.style4 .author-name:after,
        .xs-review-box .xs-review .xs-btn,
        .sidebar .widget-title:before,
        .not-found .input-group-btn,
        .ts-product-slider.owl-carousel .owl-nav .owl-prev:hover, .ts-product-slider.owl-carousel .owl-nav .owl-next:hover,
        .woocommerce ul.products li.product .button,.woocommerce ul.products li.product .added_to_cart,
        .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current,
        .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,.sponsor-web-link a:hover i, .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
        .woocommerce span.onsale,
        .navbar-light .navbar-toggler,
        .ts-contact-form .form-group .btn-submit:hover,
        .woocommerce table.cart td.actions button.button,
        .woocommerce a.button, .woocommerce button.button.alt,
        .faq .elementor-accordion .elementor-accordion-item .elementor-tab-title.elementor-active .elementor-accordion-icon,
        .woocommerce ul.products li.product .added_to_cart:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover,.woocommerce .widget_price_filter .ui-slider .ui-slider-handle{
            background: #e7272d;
        }


        .btn-primary,
        .sidebar .widget.widget_search .form-control:focus,
        .not-found .form-control:focus,
        .ts-contact-form .form-group .btn-submit:hover,
        .woocommerce div.product .woocommerce-tabs ul.tabs li.active{
            border-color: #e7272d;
        }

      

        .copyright .footer-social li a i:hover,
        .copyright .copyright-text a,
        .header .navbar-container .navbar-light .main-menu li a:hover,
        .header .navbar-container .navbar-light .main-menu li.active > a,
        .post .entry-header .entry-title a:hover,
        a:hover,
        .ts-footer .footer-menu li a:hover,
        .tag-lists a:hover, .tagcloud a:hover,
        .post-navigation span:hover, .post-navigation h3:hover,
        #rev_slider_4_1  .gloreya .tp-bullet.selected:after,
        #rev_slider_4_1 .gloreya.tparrows::before,
        .woocommerce ul.products li.product .woocommerce-loop-product__title:hover{
            color: #e7272d;
        }

        .footer-widget p strong a,
        .nav-classic-transparent.header .navbar-container .navbar-light .main-menu li a:hover{
            color: #ffe119;

        }

        .header-book-btn .btn-primary:hover,
        .btn-primary:hover,
        .xs-review-box .xs-review .xs-btn:hover,
        .nav-classic-transparent .header-cart .cart-link a sup{
            background: #ffe119;
        }
        .header-book-btn .btn-primary:hover,
        .btn-primary:hover{
            border-color: #ffe119;
        }

        

        
         .header-book-btn .btn-primary{
            background: #e7272d;
            border-color: #e7272d;
        }
         
         .header-book-btn .btn-primary{
            color: #fff;
        }
         
            .header .navbar-container .navbar-light .main-menu > li > a,
            .header-transparent .header-nav-right-info li{
             font-family:"Barlow";font-size:14px;font-weight:700;
         }

        .ts-footer{
            background-color: #1b1b1b;
            padding-top:80px;
            background-image:url(web/wp-content/uploads/2019/10/footer_bg.png);;
            background-repeat: no-repeat;
            background-size: cover;
        }
        
        

      .copyright .copyright-text{
         color: #aaa;
      }

     
</style>
<link rel='stylesheet' id='ekit-widget-styles-css'  href='{{ ("web/wp-content/plugins/elementskit-lite/widgets/init/assets/css/widget-styles77e6.css?ver=2.2.1") }}' type='text/css' media='all' />
<link rel='stylesheet' id='ekit-responsive-css'  href='{{ ("web/wp-content/plugins/elementskit-lite/widgets/init/assets/css/responsive77e6.css?ver=2.2.1") }}' type='text/css' media='all' />
<link rel='stylesheet' id='google-fonts-1-css'  href='{{ ("web/web/fonts.googleapis.com/css7476.css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPermanent+Marker%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;ver=5.7") }}' type='text/css' media='all' />
<script type='text/javascript' src='{{ ("web/wp-includes/js/jquery/jquery.min9d52.js?ver=3.5.1") }}' id='jquery-core-js'></script>
<script type='text/javascript' src='{{ ("web/wp-includes/js/jquery/jquery-migrate.mind617.js?ver=3.3.2") }}' id='jquery-migrate-js'></script>
<script type='text/javascript' src='{{ ("web/wp-content/plugins/revslider/public/assets/js/rbtools.minfab5.js?ver=6.3.8") }}' id='tp-tools-js'></script>
<script type='text/javascript' src='{{("web/wp-content/plugins/revslider/public/assets/js/rs6.minfab5.js?ver=6.3.8")}}' id='revmin-js'></script>
<script type='text/javascript' src='{{ ("web/wp-content/plugins/wp-ultimate-review/assets/public/script/content-pagee23c.js?ver=5.7") }}' id='wur_review_content_script-js'></script>
<script type='text/javascript' src='{{ ("web/wp-content/themes/gloreya/core/parallax/assets/js/jarallax7406.js?ver=2.0.1") }}' id='jarallax-js'></script>
<link rel="https://api.w.org/" href="{{ ('web/wp-json/index.html') }}" />
<link rel="alternate" type="application/json" href="{{ ('web/wp-json/wp/v2/pages/579.json') }}" />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="{{ ('web/xmlrpc0db0.php?rsd') }}" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="{{ ('web/wp-includes/wlwmanifest.xml') }}" /> 
<meta name="generator" content="WordPress 5.7" />
<meta name="generator" content="WooCommerce 5.1.0" />
<link rel="canonical" href="{{ ('index.html') }}" />
<link rel='shortlink' href='{{ ("web/indexc74d.html?p=579") }}' />
<link rel="alternate" type="application/json+oembed" href="{{ ('web/wp-json/oembed/1.0/embed61f2.json?url=https%3A%2F%2Fwww.qfacilperu.com%2Fmenu%2F') }}" />
<link rel="alternate" type="text/xml+oembed" href="{{ ('web/wp-json/oembed/1.0/embedb539?url=https%3A%2F%2Fwww.qfacilperu.com%2Fmenu%2F&amp;format=xml') }}" />

<ul class="wpc_cart_block"><a href="#" class="wpc_cart_icon">
        <i class="wpcafe-cart_icon"></i>
        <sup class="basket-item-count" style="display: inline-block;">
            <span class="cart-items-count count" id="wpc-mini-cart-count">
            </span>
        </sup>
    </a>
    <li class="wpc-menu-mini-cart wpc_background_color">
        <div class="widget_shopping_cart_content">
         
	<p class="woocommerce-mini-cart__empty-message">No hay productos en el carrito.</p>
        </div>
    </li>
</ul>
			<script type="text/javascript">
				var elementskit_module_parallax_url = "{{ ('web/wp-content/themes/gloreya/core/parallax/index.html') }}"
			</script>
			<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta name="generator" content="Powered by Slider Revolution 6.3.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<link rel="icon" href="{{ ('web/wp-content/uploads/2021/03/cropped-QFacil-1-32x32.png') }}" sizes="32x32" />
<link rel="icon" href="{{ ('web/wp-content/uploads/2021/03/cropped-QFacil-1-192x192.png') }}" sizes="192x192" />
<link rel="apple-touch-icon" href="{{ ('web/wp-content/uploads/2021/03/cropped-QFacil-1-180x180.png') }}" />
<meta name="msapplication-TileImage" content="{{ ('https://www.qfacilperu.com/wp-content/uploads/2021/03/cropped-QFacil-1-270x270.png') }}" />
<script type="text/javascript">function setREVStartSize(e){
			//window.requestAnimationFrame(function() {				 
				window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;	
				window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;	
				try {								
					var pw = document.getElementById(e.c).parentNode.offsetWidth,
						newh;
					pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
					e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
					e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
					e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
					e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
					e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
					e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
					e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
					if(e.layout==="fullscreen" || e.l==="fullscreen") 						
						newh = Math.max(e.mh,window.RSIH);					
					else{					
						e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
						for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
						e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
						e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
						for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
											
						var nl = new Array(e.rl.length),
							ix = 0,						
							sl;					
						e.tabw = e.tabhide>=pw ? 0 : e.tabw;
						e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
						e.tabh = e.tabhide>=pw ? 0 : e.tabh;
						e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
						for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
						sl = nl[0];									
						for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
						var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					
						newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
					}				
					if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));					
					document.getElementById(e.c).height = newh+"px";
					window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";				
				} catch(e){
					console.log("Failure at Presize of Slider:" + e)
				}					   
			//});
		  };</script>
		<style type="text/css" id="wp-custom-css">
			
         .menu-block .inner-box .text {
            padding-right: 50px;
         }
         .intro-content-section{
            overflow: hidden;
         }
         .subtitle{
               position: relative;
         }
         .subtitle::after{
               width: 100px;
               height: 2px;
               background: #e7272d;
               right: 0;
               margin-left: 30px;
               content: '';
               display: inline-block;
               vertical-align: middle;
            }		
      </style>
</head>

<body class="page-template page-template-template page-template-full-width-template page-template-templatefull-width-template-php page page-id-579 theme-gloreya woocommerce-no-js sidebar-active sidebar-class elementor-default elementor-kit-1489 elementor-page elementor-page-579">
	
<!-- header nav start-->
   <header id="header" class="header header-standard  navbar-sticky">

   <!-- navbar container start -->
      <div class="navbar-container">
         <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
               <a class="navbar-brand" href="/">
                  <img src="{{ ('img/web/logo.png') }}" alt="QFacil Perú">
               </a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#primary-nav" aria-controls="primary-nav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon">
                     <i class="icon icon-menu"></i>
                  </span>
               </button>   

               <div id="primary-nav" class="collapse navbar-collapse">
                  <ul id="main-menu" class="navbar-nav  main-menu">
                     <li id="menu-item-1020" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-1020 nav-item">
                        <a href="/" class="nav-link">Inicio</a>
                     </li>
                     <li id="menu-item-1021" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-579 current_page_item menu-item-1021 nav-item active">
                        <a href="index.html" class="nav-link active">Servicios</a>
                     </li>
                     <li id="menu-item-1032" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1032 nav-item dropdown">
                        <a href="web/shop/index.html" class="nav-link dropdown-toggle" data-toggle="dropdown">Tiendas</a>
                        <ul class="dropdown-menu">
                           <li id="menu-item-1180" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1180 nav-item">
                              <a href="web/shop/index.html" class=" dropdown-item">Tienda 1</a>
                           </li>
                        </ul>
                     </li>
                     <li id="menu-item-1022" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1022 nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Nosotros</a>
                        <ul class="dropdown-menu">
                           <li id="menu-item-927" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-927 nav-item">
                              <a href="web/about-us/index.html" class=" dropdown-item">Nosotros</a>
                           </li>
                           <li id="menu-item-926" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-926 nav-item">
                              <a href="web/team/index.html" class=" dropdown-item">Nuestro Equipo</a>
                           </li>
                           <li id="menu-item-1026" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1026 nav-item">
                              <a href="web/blog/index.html" class=" dropdown-item">Blog</a>
                           </li>
                           <li id="menu-item-1024" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1024 nav-item">
                              <a href="web/testimonial/index.html" class=" dropdown-item">Comentarios</a>
                           </li>
                           <li id="menu-item-1023" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1023 nav-item">
                              <a href="web/gallery/index.html" class=" dropdown-item">Galeria</a>
                           </li>	
                           <li id="menu-item-925" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-925 nav-item">
                              <a href="web/faq/index.html" class=" dropdown-item">Preguntas</a>
                           </li>
                           <li id="menu-item-1513" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1513 nav-item">
                              <a href="web/trabaja-con-nostros/index.html" class=" dropdown-item">Trabaja con Nostros</a>
                           </li>
                        </ul>
                     </li>
                     <li id="menu-item-924" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-924 nav-item">
                        <a href="web/contact/index.html" class="nav-link">Contáctenos</a>
                     </li>
                     <li id="menu-item-924" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-924 nav-item">
                        <a href="web/contact/index.html" class="nav-link">Ingresar</a>
                     </li>
                  </ul>
               </div>

                     <!-- collapse end -->
               <ul class="header-nav-right-info form-inline">
                  <li class="header-book-btn">
                     <a href="web/shop/index.html" class="btn btn-primary">PEDIDOS</a>
                  </li>
                  <li>
                     <div class="header-cart">
                        <div class="cart-link">
                           <a class="cart-contents" href="web/cart/index.html" title="View your shopping cart">
                              <span class="icon icon-tscart"></span>
                              <sup>0</sup>
                           </a>
                        </div>
                     </div>
                  </li>
                  <!-- off canvas -->
                  <li>
                     <a href="#" class="navSidebar-button">
                        <i class="icon icon-humburger"></i>
                     </a>
                  </li>
               </ul>
            </nav>
                  <!-- nav end -->
         </div>
               <!-- container end -->
      </div>
         <!-- navbar contianer end -->
   </header>

<!-- sidebar cart item -->
   <div class="xs-sidebar-group info-group">
      <div class="xs-overlay xs-bg-black"></div>
         <div class="xs-sidebar-widget">
         <div class="sidebar-widget-container">
               <div class="widget-heading">
                  <a href="#" class="close-side-widget">
                     <i class="icon icon-cross"></i>
                  </a>
               </div>
               <div class="sidebar-textwidget">
                  <div class="sidebar-logo-wraper">
                     <a class="navbar-brand" href="web/index.html">
                        <img src="web/wp-content/uploads/2021/03/Q%c2%b4Facil.png" alt="QFacil Perú">
                     </a>
                  </div>
                  <div class="off-canvas-desc">
                        Esto es una prueba de la misión de la empresa.                
                  </div>
                  <ul class="sideabr-list-widget">
                     <li>
                        <div class="media">
                           <div class="d-flex">
                              <i class="fa fa-envelope"></i>
                           </div>
                           <div class="media-body">
                              <span>ventas@qfacil.com</span>
                           </div>
                        </div><!-- address 1 -->
                     </li>
                        
                     <li>
                        <div class="media">
                           <div class="d-flex">
                              <i class="fa fa-phone"></i>
                           </div>
                           <div class="media-body">
                              <span>+51944646613</span>
                           </div>
                        </div><!-- address 1 -->
                     </li>
                  </ul>
                  <!-- .sideabr-list-widget -->

                  <ul class="social-list version-2">
                        <li class="ts-facebook">
                           <a title="facebook" href="#">
                              <i class="fa fa-facebook"></i>
                           </a>
                        </li>
                        <li class="ts-twitter">
                           <a title="Twitter" href="#">
                              <i class="fa fa-twitter"></i>
                           </a>
                        </li>
                        <li class="ts-linkedin">
                           <a title="Linkedin" href="#">
                              <i class="fa fa-linkedin"></i>
                           </a>
                        </li>
                        <li class="ts-youtube-play">
                           <a title="youtube" href="#">
                              <i class="fa fa-youtube-play"></i>
                           </a>
                        </li>
                  </ul>
               </div>
         </div>
      </div>
   </div>    <!-- END sidebar widget item -->    <!-- END offset cart strart -->

   <section role="main" class="content-body">

      @yield('contenido')

   </section>


  

 
   
<footer class="ts-footer solid-bg-two" >
   <div class="container">
      <div class="footer-logo-area text-center">
         <a class="footer-logo" href="web/index.html">
            <img src="web/wp-content/uploads/2021/03/Q%c2%b4Facil-1.png" alt="QFacil Perú">
         </a>
      </div>

      <div class="row">
         <div class="col-lg-3 col-md-6">
            <div class="footer-widget footer-left-widget">
               <h3 class="widget-title">Address</h3>			
               <div class="textwidget">
                  <p>570 8th Ave, <br /> New York, NY 10018<br /> United States</p>
               </div>
            </div>                    
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="footer-widget footer-two-widget">
               <h3 class="widget-title">Book a table</h3>			
               <div class="textwidget">
                  <p>Dogfood och Sliders foodtruck. <br /> Under Om oss kan ni läsa<br />
                     <strong>
                        <a href="tel:123-456-7890">(850) 435-4155</a>
                     </strong>
                  </p>
               </div>
            </div>                     
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="footer-widget footer-three-widget">
               <h3 class="widget-title">Opening hours</h3>			
               <div class="textwidget">
                  <p>Monday &#8211; Friday <br /> 10.00 AM &#8211; 11.00 PM</p>
               </div>
            </div>                     
         </div>
         <div class="col-lg-3 col-md-6">
            <h3 class="widget-title">Instagram feed</h3>        
            <div class="instagram_photo">
               <div id="instafeed" class="feed-content" data-token="2367672995.1677ed0.dea7a14501d04cd9982c7a0d23c716dd" data-media-count="3"></div>  
            </div>
         </div>                     
      </div>
<!-- end col -->
   </div>
   <div class='footer-bar'> </div>

   <div class="row copyright">
      <div class="col-lg-6 col-md-7 align-self-center">
         <div class="copyright-text">
            <div id="footer-nav" class="menu-footer-menu-container">
               <ul id="footer-menu" class="footer-menu">
                  <li id="menu-item-1309" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1309 nav-item">
                     <a href="web/about-us/index.html" class="nav-link">Nosotros</a>
                  </li>
                  <li id="menu-item-1308" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-579 current_page_item menu-item-1308 nav-item active">
                     <a href="index.html" class="nav-link active">Menu</a>
                  </li>
                  <li id="menu-item-1305" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1305 nav-item">
                     <a href="web/blog/index.html" class="nav-link">Blog</a>
                  </li>
                  <li id="menu-item-1304" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1304 nav-item">
                     <a href="web/gallery/index.html" class="nav-link">Galeria</a>
                  </li>
                  <li id="menu-item-1310" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1310 nav-item">
                     <a href="web/faq/index.html" class="nav-link">Preguntas Frecuentes</a>
                  </li>
                  <li id="menu-item-1306" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1306 nav-item">
                     <a href="web/contact/index.html" class="nav-link">Contactenos</a>
                  </li>
               </ul>
            </div>    
            &copy; 2019, Gloreya. All rights reserved                        </div>
         </div>
         <div class="col-lg-6 col-md-5 align-self-center">
            <div class="footer-social">
               <ul>
                  <li class="ts-facebook">
                     <a href="#" target="_blank">
                        <i class="fa fa-facebook"></i>
                     </a>
                  </li>
                  <li class="ts-twitter">
                     <a href="#" target="_blank">
                        <i class="fa fa-twitter"></i>
                     </a>
                  </li>
                  <li class="ts-instagram">
                     <a href="#" target="_blank">
                        <i class="fa fa-instagram"></i>
                     </a>
                  </li>
                  <li class="ts-youtube-play">
                     <a href="#" target="_blank">
                        <i class="fa fa-youtube-play"></i>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</footer>
        <!-- end footer -->
<div class="BackTo">
   <a href="#" class="fa fa-angle-up" aria-hidden="true"></a>
</div>
   
<script type="text/javascript">
   (function () {
   var c = document.body.className;
   c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
   document.body.className = c;
   })();
</script>
<style id='wpc-cart-css-inline-css' type='text/css'>
   .wpc_background_color { background-color : #d5d5d5 }.wpc_cart_block .wpc_background_color a.button.wc-forward { background-color : #5D78FF}a.wpc_cart_icon i { color : #fff}.wpc_cart_block .wpc_background_color a.button.wc-forward:hover { background-color : #5D78FF}.wpc_cart_block .woocommerce-mini-cart li .remove.remove_from_cart_button:hover { background-color : #5D78FF}.cart-items-count { color : #fff}
</style>
<script type='text/javascript' src='web/wp-includes/js/dist/vendor/wp-polyfill.min89b1.js?ver=7.4.4' id='wp-polyfill-js'></script>
<script type='text/javascript' id='wp-polyfill-js-after'>
   ( 'fetch' in window ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-fetch.min6e0e.js?ver=3.0.0"></scr' + 'ipt>' );( document.contains ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-node-contains.min2e00.js?ver=3.42.0"></scr' + 'ipt>' );( window.DOMRect ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-dom-rect.min2e00.js?ver=3.42.0"></scr' + 'ipt>' );( window.URL && window.URL.prototype && window.URLSearchParams ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-url.min5aed.js?ver=3.6.4"></scr' + 'ipt>' );( window.FormData && window.FormData.prototype.keys ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-formdata.mine9bd.js?ver=3.0.12"></scr' + 'ipt>' );( Element.prototype.matches && Element.prototype.closest ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-element-closest.min4c56.js?ver=2.0.2"></scr' + 'ipt>' );( 'objectFit' in document.documentElement.style ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-object-fit.min531b.js?ver=2.3.4"></scr' + 'ipt>' );
</script>
<script type='text/javascript' src='web/wp-includes/js/dist/hooks.minf521.js?ver=50e23bed88bcb9e6e14023e9961698c1' id='wp-hooks-js'></script>
<script type='text/javascript' src='web/wp-includes/js/dist/i18n.min87d6.js?ver=db9a9a37da262883343e941c3731bc67' id='wp-i18n-js'></script>
<script type='text/javascript' id='wp-i18n-js-after'>
   wp.i18n.setLocaleData( { 'text direction\u0004ltr': [ 'ltr' ] } );
</script>
<script type='text/javascript' src='web/wp-includes/js/dist/vendor/lodash.minf492.js?ver=4.17.19' id='lodash-js'></script>
<script type='text/javascript' id='lodash-js-after'>
   window.lodash = _.noConflict();
</script>
<script type='text/javascript' src='web/wp-includes/js/dist/url.minacd8.js?ver=0ac7e0472c46121366e7ce07244be1ac' id='wp-url-js'></script>
<script type='text/javascript' id='wp-api-fetch-js-translations'>
   ( function( domain, translations ) {
      var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
      localeData[""].domain = domain;
      wp.i18n.setLocaleData( localeData, domain );
   } )( "default", {"translation-revision-date":"2021-03-17 08:03:15+0000","generator":"GlotPress\/3.0.0-alpha.2","domain":"messages","locale_data":{"messages":{"":{"domain":"messages","plural-forms":"nplurals=2; plural=n != 1;","lang":"es"},"You are probably offline.":["Probablemente est\u00e1s desconectado."],"Media upload failed. If this is a photo or a large image, please scale it down and try again.":["La subida de medios ha fallado. Si esto es una foto o una imagen grande, por favor, reduce su tama\u00f1o e int\u00e9ntalo de nuevo."],"The response is not a valid JSON response.":["Las respuesta no es una respuesta JSON v\u00e1lida."],"An unknown error occurred.":["Ha ocurrido un error desconocido."]}},"comment":{"reference":"wp-includes\/js\/dist\/api-fetch.js"}} );
</script>
<script type='text/javascript' src='web/wp-includes/js/dist/api-fetch.minf3b9.js?ver=a783d1f442d2abefc7d6dbd156a44561' id='wp-api-fetch-js'></script>
<script type='text/javascript' id='wp-api-fetch-js-after'>
   wp.apiFetch.use( wp.apiFetch.createRootURLMiddleware( "web/wp-json/index.html" ) );
   wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware( "f4ee650c5f" );
   wp.apiFetch.use( wp.apiFetch.nonceMiddleware );
   wp.apiFetch.use( wp.apiFetch.mediaUploadMiddleware );
   wp.apiFetch.nonceEndpoint = "web/wp-admin/admin-ajaxf809.html?action=rest-nonce";
</script>
<script type='text/javascript' id='contact-form-7-js-extra'>
   /* <![CDATA[ */
   var wpcf7 = {"cached":"1"};
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/contact-form-7/includes/js/index91d5.js?ver=5.4' id='contact-form-7-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70' id='jquery-blockui-js'></script>
<script type='text/javascript' id='wc-add-to-cart-js-extra'>
   /* <![CDATA[ */
   var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"Ver carrito","cart_url":"https:\/\/www.qfacilperu.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min0e7d.js?ver=5.1.0' id='wc-add-to-cart-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4' id='js-cookie-js'></script>
<script type='text/javascript' id='woocommerce-js-extra'>
   /* <![CDATA[ */
   var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min0e7d.js?ver=5.1.0' id='woocommerce-js'></script>
<script type='text/javascript' id='wc-cart-fragments-js-extra'>
   /* <![CDATA[ */
   var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_3bc0025c17005dfd13ef6e85d4584378","fragment_name":"wc_fragments_3bc0025c17005dfd13ef6e85d4584378","request_timeout":"5000"};
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min0e7d.js?ver=5.1.0' id='wc-cart-fragments-js'></script>
<script type='text/javascript' id='wc-cart-fragments-js-after'>
		jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
			var jetpackLazyImagesLoadEvent;
			try {
				jetpackLazyImagesLoadEvent = new Event( 'jetpack-lazy-images-load', {
					bubbles: true,
					cancelable: true
				} );
			} catch ( e ) {
				jetpackLazyImagesLoadEvent = document.createEvent( 'Event' )
				jetpackLazyImagesLoadEvent.initEvent( 'jetpack-lazy-images-load', true, true );
			}
			jQuery( 'body' ).get( 0 ).dispatchEvent( jetpackLazyImagesLoadEvent );
		} );
		
</script>
<script type='text/javascript' id='mailchimp-woocommerce-js-extra'>
   /* <![CDATA[ */
   var mailchimp_public_data = {"site_url":"https:\/\/www.qfacilperu.com","ajax_url":"https:\/\/www.qfacilperu.com\/wp-admin\/admin-ajax.php","language":"es"};
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/mailchimp-for-woocommerce/public/js/mailchimp-woocommerce-public.min9776.js?ver=2.5.1' id='mailchimp-woocommerce-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/moment.mine7f0.js?ver=1.3.1' id='wpc-moment-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/flatpickr.mine7f0.js?ver=1.3.1' id='wpc-flatpicker-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/jquery.timepicker.mine7f0.js?ver=1.3.1' id='wpc-jquery-timepicker-js'></script>
<script type='text/javascript' id='wpc-public-js-extra'>
   /* <![CDATA[ */
   var wpc_form_client_data = "{\"settings\":[],\"wpc_ajax_url\":\"https:\\\/\\\/www.qfacilperu.com\\\/wp-admin\\\/admin-ajax.php\"}";
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/wpc-publice7f0.js?ver=1.3.1' id='wpc-public-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/commone7f0.js?ver=1.3.1' id='wpc-common-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/TweenMax.min7406.js?ver=2.0.1' id='tweenmax-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/jquery.easing.1.37406.js?ver=2.0.1' id='jquery-easing-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/tilt.jquery.min7406.js?ver=2.0.1' id='tilt-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/anime7406.js?ver=2.0.1' id='animejs-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/magician7406.js?ver=2.0.1' id='magicianjs-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementskit-lite/libs/framework/assets/js/frontend-script77e6.js?ver=2.2.1' id='elementskit-framework-js-frontend-js'></script>
<script type='text/javascript' id='elementskit-framework-js-frontend-js-after'>
   var elementskit = {
         resturl: 'https://www.qfacilperu.com/wp-json/elementskit/v1/',
      }
</script>
<script type='text/javascript' src='web/wp-content/plugins/elementskit-lite/widgets/init/assets/js/widget-scripts77e6.js?ver=2.2.1' id='ekit-widget-scripts-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/bootstrap.min7406.js?ver=2.0.1' id='bootstrap-min-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/popper.min7406.js?ver=2.0.1' id='popper-min-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/jquery.magnific-popup.min7406.js?ver=2.0.1' id='jquery-magnific-popup-min-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/instafeed.min7406.js?ver=2.0.1' id='instafeed-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/owl.carousel.min7406.js?ver=2.0.1' id='owl-carousel-min-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/jquery.easypiechart.min7406.js?ver=2.0.1' id='jquery-easypiechart-min-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/script7406.js?ver=2.0.1' id='gloreya-script-js'></script>
<script type='text/javascript' src='web/wp-includes/js/wp-embed.mine23c.js?ver=5.7' id='wp-embed-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/js/webpack.runtime.minaeb9.js?ver=3.1.4' id='elementor-webpack-runtime-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/js/frontend-modules.minaeb9.js?ver=3.1.4' id='elementor-frontend-modules-js'></script>
<script type='text/javascript' src='web/wp-includes/js/jquery/ui/core.min35d0.js?ver=1.12.1' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/lib/dialog/dialog.mina288.js?ver=4.8.1' id='elementor-dialog-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min05da.js?ver=4.0.2' id='elementor-waypoints-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/lib/share-link/share-link.minaeb9.js?ver=3.1.4' id='share-link-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/lib/swiper/swiper.min48f5.js?ver=5.3.6' id='swiper-js'></script>
<script type='text/javascript' id='elementor-frontend-js-before'>
   var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false,"isImprovedAssetsLoading":false},"i18n":{"shareOnFacebook":"Compartir en Facebook","shareOnTwitter":"Compartir en Twitter","pinIt":"Pinear","download":"Descargar","downloadImage":"Descargar imagen","fullscreen":"Pantalla completa","zoom":"Zoom","share":"Compartir","playVideo":"Reproducir v\u00eddeo","previous":"Anterior","next":"Siguiente","close":"Cerrar"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.1.4","is_static":false,"experimentalFeatures":[],"urls":{"assets":"https:\/\/www.qfacilperu.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":579,"title":"Menu%20%E2%80%93%20QFacil%20Per%C3%BA","excerpt":"","featuredImage":false}};
</script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/js/frontend.minaeb9.js?ver=3.1.4' id='elementor-frontend-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/elementore7f0.js?ver=1.3.1' id='wpc-elementor-frontend-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/elementor7406.js?ver=2.0.1' id='gloreya-main-elementor-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementskit-lite/widgets/init/assets/js/slick.min77e6.js?ver=2.2.1' id='ekit-slick-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementskit-lite/widgets/init/assets/js/elementor77e6.js?ver=2.2.1' id='elementskit-elementor-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/widget-init7406.js?ver=2.0.1' id='elementskit-parallax-widget-init-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/section-init7406.js?ver=2.0.1' id='elementskit-parallax-section-init-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/js/preloaded-elements-handlers.minaeb9.js?ver=3.1.4' id='preloaded-elements-handlers-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementskit-lite/modules/controls/assets/js/widgetarea-editor77e6.js?ver=2.2.1' id='elementskit-js-widgetarea-control-editor-js'></script>

</body>
</html>