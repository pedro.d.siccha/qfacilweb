<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>Nosotros | QFácil</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="{{ ('img/web/icono.ico') }}" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:100,300,300i,400,500,600,700,900%7CRaleway:500">
    <link rel="stylesheet" href="{{ ('web/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ ('web/css/fonts.css') }}">
    <link rel="stylesheet" href="{{ ('web/css/style.css') }}">
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="{{ ('js/html5shiv.min.js') }}"></script>
    <![endif]-->
  </head>
  <body>
    <div class="preloader">
      <div class="wrapper-triangle">
        <div class="pen">
            <img src="img/web/carga.png" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">    
          <div class="line-triangle">
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
          </div>
          <div class="line-triangle">
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
          </div>
          <div class="line-triangle">
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="page">
      <!-- Top Banner-->
      <!-- Page Header-->
      <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-modern" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="56px" data-xl-stick-up-offset="56px" data-xxl-stick-up-offset="56px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-inner-outer">
              <div class="rd-navbar-inner">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                    <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap">
                      <span></span>
                    </button>
                  <!-- RD Navbar Brand-->
                    <div class="rd-navbar-brand">
                        <a class="brand" href="{{ Route('home') }}">
                            <img class="brand-logo-dark" src="{{ ('img/web/logo.png') }}" alt="" width="198" height="66"/>
                        </a>
                    </div>
                </div>
                <div class="rd-navbar-right rd-navbar-nav-wrap">
                  <div class="rd-navbar-aside">
                    <ul class="rd-navbar-contacts-2">
                      <li>
                        <div class="unit unit-spacing-xs">
                          <div class="unit-left">
                              <span class="icon mdi mdi-phone"></span>
                            </div>
                            <div class="unit-body">
                              <a class="phone" href="tel:#">944646619</a>
                            </div>
                        </div>
                      </li>
                      <li>
                        <div class="unit unit-spacing-xs">
                            <div class="unit-left">
                              <span class="icon mdi mdi-map-marker"></span>
                            </div>
                            <div class="unit-body">
                                <a class="address" href="#">Ciudad de Lima</a>
                            </div>
                        </div>
                      </li>
                    </ul>
                    <ul class="list-share-2">
                        <li>
                          <a class="icon mdi mdi-facebook" href="#"></a>
                        </li>
                        <li>
                            <a class="icon mdi mdi-twitter" href="#"></a>
                        </li>
                        <li>
                            <a class="icon mdi mdi-instagram" href="#"></a>
                        </li>
                        <li>
                            <a class="icon mdi mdi-google-plus" href="#"></a>
                        </li>
                    </ul>
                  </div>
                  <div class="rd-navbar-main">
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                        <li class="rd-nav-item">
                            <a class="rd-nav-link" href="{{ Route('home') }}">Inicio</a>
                        </li>
                        <li class="rd-nav-item">
                            <a class="rd-nav-link" href="{{ Route('nosotros') }}">Nosotros</a>
                        </li>
                        <li class="rd-nav-item active">
                            <a class="rd-nav-link" href="{{ Route('blog') }}">Blog</a>
                        </li>
                        <li class="rd-nav-item">
                            <a class="rd-nav-link" href="{{ Route('preguntas') }}">Preguntas</a>
                        </li>
                        <li class="rd-nav-item">
                            <a class="rd-nav-link" href="{{ route('login') }}">Ingresar</a>
                        </li>
                    </ul>
                  </div>
                </div>
                <div class="rd-navbar-project-hamburger rd-navbar-project-hamburger-open rd-navbar-fixed-element-1" data-multitoggle=".rd-navbar-inner" data-multitoggle-blur=".rd-navbar-wrap" data-multitoggle-isolate="data-multitoggle-isolate">
                    <div class="project-hamburger">
                        <span class="project-hamburger-arrow"></span>
                        <span class="project-hamburger-arrow"></span>
                        <span class="project-hamburger-arrow"></span>
                    </div>
                </div>
                <div class="rd-navbar-project">
                    <div class="rd-navbar-project-header">
                      <h5 class="rd-navbar-project-title">Tiendas</h5>
                      <div class="rd-navbar-project-hamburger rd-navbar-project-hamburger-close" data-multitoggle=".rd-navbar-inner" data-multitoggle-blur=".rd-navbar-wrap" data-multitoggle-isolate="data-multitoggle-isolate">
                          <div class="project-close">
                              <span></span>
                              <span></span>
                          </div>
                      </div>
                    </div>
                    <div class="rd-navbar-project-content rd-navbar-content">
                      <div>
                        <div class="row gutters-20" data-lightgallery="group">
                          @foreach ($tienda as $t)
                          <div class="col-6">
                            <!-- Thumbnail Creative-->
                            <article class="thumbnail thumbnail-creative">
                              <a href="{{ $t->foto }}" data-lightgallery="item">
                                <div class="thumbnail-creative-figure">
                                    <img src="{{ $t->foto }}" alt="" width="195" height="164"/>
                                </div>
                                <div class="thumbnail-creative-caption">
                                    <span class="icon thumbnail-creative-icon linearicons-magnifier"></span>
                                </div>
                              </a>
                          </article>    
                          </div>
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <!-- parallax top-->
      <!-- Breadcrumbs -->
      <section class="bg-gray-7">
        <div class="breadcrumbs-custom box-transform-wrap context-dark">
          <div class="container">
            <h3 class="breadcrumbs-custom-title">BLOG</h3>
            <div class="breadcrumbs-custom-decor"></div>
          </div>
          <div class="box-transform" style="background-image: url(web/images/bg-1.jpg);"></div>
        </div>
        <div class="container">
          <ul class="breadcrumbs-custom-path">
            <li><a href="{{ Route('home') }}">Inicio</a></li>
            <li class="active">Blog</li>
          </ul>
        </div>
      </section>
      
    <section id="main-content" class="blog main-container" role="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <article class="post-354 post type-post status-publish format-standard has-post-thumbnail hentry category-burger">
                        <div class="post-media post-image">
                            <a href="../2019/10/10/labor-depar-rules-pro-as-tweaks-overtime/index.html">
                                <img class="img-fluid" src="web/images/wp-say-669x447.jpg" alt=" Labor Depar rules pro as tweaks overtime">
                            </a>
                
                        </div>
                        <div class="post-body clearfix">
                            <div class="entry-header">
                                <div class="post-meta">
                                    <span class="post-author">
                                        <i class="icon icon-user"></i> 
                                        <a href="../author/admin/index.html">admin</a>
                                    </span>
                                    <span class="post-meta-date">
                                        <i class="icon icon-clock"></i>
                                        10 de octubre de 2019</span><span class="meta-categories post-cat">
                                        <i class="icon icon-folder"></i>
                                        <a href="../category/burger/index.html" rel="category tag">Burger</a> 
                                    </span>	
                                </div>
                                <h2 class="entry-title">
                                    <a href="../2019/10/10/labor-depar-rules-pro-as-tweaks-overtime/index.html">Labor Depar rules pro as tweaks overtime</a>
                                </h2>
                            </div>
                            <div class="post-content">
                                <div class="entry-content">
                                    <p>
                                        You’re cooking a meal, especially a holiday meal, to be served to friends or family, the key to success is planning. Don’t run around second guessing yourself and what you’re going to make. Plan your menu, do the shopping, and&hellip;            
                                    </p>
                                </div>
                                <div class="post-footer readmore-btn-area">
                                    <a class="readmore" href="../2019/10/10/labor-depar-rules-pro-as-tweaks-overtime/index.html">Continue 
                                        <i class="icon icon-arrow-right"></i>
                                    </a>
                                </div>      
                            </div>
                        </div>
         
                    </article>                                
                </div><!-- .col-md-8 -->
    
                <div class="col-lg-4 col-md-12">
                    <aside id="sidebar" class="sidebar" role="complementary">
                        <div id="search-2" class="widget widget_search">
                            <form  method="get" action="https://www.qfacilperu.com/" class="gloreya-serach">
                                <div class="input-group">
                                    <input type="search" class="form-control" name="s" placeholder="Search" value="">
                                    <span class="input-group-btn">
                                        <i class="fa fa-search"></i>
                                    </span>
                                </div>
                            </form>
                        </div>
                        <div id="recent-posts-2" class="widget widget_recent_entries">
                            <h4 class="widget-title">Entradas recientes</h4>
                            <ul>
                                <li>
                                    <a href="../2019/10/10/labor-depar-rules-pro-as-tweaks-overtime/index.html">Labor Depar rules pro as tweaks overtime</a>
                                </li>
                            </ul>
                        </div>
                        <div id="recent-comments-2" class="widget widget_recent_comments">
                            <h4 class="widget-title">Comentarios recientes</h4>
                            <ul id="recentcomments">
                                <li class="recentcomments">
                                    <span class="comment-author-link">admin</span> 
                                    en 
                                    <a href="../product/baked-beer-pizza-copy/index.html#comment-15">Burger &038; Pasta</a>
                                </li>
                            </ul>
                        </div>
                        <div id="archives-2" class="widget widget_archive">
                            <h4 class="widget-title">Archivos</h4>
                            <ul>
                                <li>
                                    <a href='../2019/10/index.html'>octubre 2019</a>
                                </li>
                            </ul>
                        </div>
                        <div id="categories-2" class="widget widget_categories">
                            <h4 class="widget-title">Categorías</h4>
                            <ul>
                                <li class="cat-item cat-item-19">
                                    <a href="../category/burger/index.html">Burger</a>
                                </li>
                            </ul>
                        </div>
                        <div id="meta-2" class="widget widget_meta">
                            <h4 class="widget-title">Meta</h4>
                            <ul>
                                <li>
                                    <a href="../wp-login.html">Acceder</a>
                                </li>
                                <li>
                                    <a href="../feed/index.html">Feed de entradas</a>
                                </li>
                                <li>
                                    <a href="../comments/feed/index.html">Feed de comentarios</a>
                                </li>
                            </ul>
                        </div>      
                    </aside>
                </div>
            </div>
        </div>
    </section>

      


      <!-- Bottom Banner--><a class="section section-banner" href="https://www.templatemonster.com/intense-multipurpose-html-template.html" style="background-image: url(images/banner/background-03-1920x310.jpg); background-image: -webkit-image-set( url(images/banner/background-03-1920x310.jpg) 1x, url(images/banner/background-03-3840x620.jpg) 2x )"><img src="images/banner/foreground-03-1600x310.png" srcset="images/banner/foreground-03-1600x310.png 1x, images/banner/foreground-03-3200x620.png 2x" alt="" width="1600" height="310"></a>
      <!-- Page Footer-->
      <footer class="section footer-modern context-dark footer-modern-2">
        <div class="footer-modern-line">
          <div class="container">
            <div class="row row-50">
              <div class="col-md-6 col-lg-4">
                <h5 class="footer-modern-title oh-desktop"><span class="d-inline-block wow slideInLeft">TIENDAS</span></h5>
                <ul class="footer-modern-list d-inline-block d-sm-block wow fadeInUp">
                    @foreach ($tienda as $t)
                        <li><a href="#">{{ $t->nombre }}</a></li>      
                    @endforeach
                </ul>
              </div>
              <div class="col-md-6 col-lg-4 col-xl-3">
                <h5 class="footer-modern-title oh-desktop">
                    <span class="d-inline-block wow slideInLeft">Información</span>
                </h5>
                <ul class="footer-modern-list d-inline-block d-sm-block wow fadeInUp">
                    <li><a href="{{ Route('nosotros') }}">Nosotros</a></li>
                    <li><a href="{{ Route('blog') }}">Blog</a></li>
                    <li><a href="{{ Route('preguntas') }}">Preguntas</a></li>
                </ul>
              </div>
              <div class="col-lg-4 col-xl-5">
                <h5 class="footer-modern-title oh-desktop"><span class="d-inline-block wow slideInLeft">Noticias</span></h5>
                <p class="wow fadeInRight">Registre su correo para recibir promociones.</p>
                <!-- RD Mailform-->
                <form class="rd-form rd-mailform rd-form-inline rd-form-inline-sm oh-desktop" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                  <div class="form-wrap wow slideInUp">
                    <input class="form-input" id="subscribe-form-2-email" type="email" name="email" data-constraints="@Email @Required"/>
                    <label class="form-label" for="subscribe-form-2-email">Ingrese su correo</label>
                  </div>
                  <div class="form-button form-button-2 wow slideInRight">
                    <button class="button button-sm button-icon-3 button-primary button-winona" type="submit"><span class="d-none d-xl-inline-block">ENVIAR</span><span class="icon mdi mdi-telegram d-xl-none"></span></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-modern-line-2">
          <div class="container">
            <div class="row row-30 align-items-center">
              <div class="col-sm-6 col-md-7 col-lg-4 col-xl-4">
                <div class="row row-30 align-items-center text-lg-center">
                    <div class="col-md-7 col-xl-6">
                      <a class="brand" href="{{Route('home')}}">
                          <img src="{{ ('img/web/LogoFooter.png') }}" alt="" width="198" height="66"/>
                        </a>
                    </div>
                  <div class="col-md-5 col-xl-6">
                    <div class="iso-1"><span><img src="{{ ('img\web\playstore.png') }}" alt="" width="58" height="25"/></span><span class="iso-1-big"><img src="{{ ('img\web\appstore.png') }}" alt="" width="58" height="25"/></span></div>
                    
                  </div>
                  
                </div>
              </div>
              <div class="col-sm-6 col-md-12 col-lg-8 col-xl-8 oh-desktop">
                <div class="group-xmd group-sm-justify">
                  <div class="footer-modern-contacts wow slideInUp">
                    <div class="unit unit-spacing-sm align-items-center">
                      <div class="unit-left"><span class="icon icon-24 mdi mdi-phone"></span></div>
                      <div class="unit-body"><a class="phone" href="tel:#">944646619</a></div>
                    </div>
                  </div>
                  <div class="footer-modern-contacts wow slideInDown">
                    <div class="unit unit-spacing-sm align-items-center">
                      <div class="unit-left"><span class="icon mdi mdi-email"></span></div>
                      <div class="unit-body"><a class="mail" href="mailto:#">info@qfacil.com</a></div>
                    </div>
                  </div>
                  <div class="wow slideInRight">
                    <ul class="list-inline footer-social-list footer-social-list-2 footer-social-list-3">
                      <li><a class="icon mdi mdi-facebook" href="#"></a></li>
                      <li><a class="icon mdi mdi-twitter" href="#"></a></li>
                      <li><a class="icon mdi mdi-instagram" href="#"></a></li>
                      <li><a class="icon mdi mdi-google-plus" href="#"></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-modern-line-3">
          <div class="container">
            <div class="row row-10 justify-content-between">
              <div class="col-md-6"><span>Ciudad de Lima</span></div>
              <div class="col-md-auto">
                <!-- Rights-->
                <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span></span><span>.&nbsp;</span><span>All Rights Reserved.</span><span> Design&nbsp;by&nbsp;<a href="">InfoRad</a></span></p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src="{{('web/js/core.min.js')}}"></script>
    <script src="{{ ('web/js/script.js') }}"></script>
  </body>
</html>