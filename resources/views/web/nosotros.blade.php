<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>Nosotros | QFácil</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="{{ ('img/web/icono.ico') }}" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:100,300,300i,400,500,600,700,900%7CRaleway:500">
    <link rel="stylesheet" href="{{ ('web/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ ('web/css/fonts.css') }}">
    <link rel="stylesheet" href="{{ ('web/css/style.css') }}">
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="{{ ('js/html5shiv.min.js') }}"></script>
    <![endif]-->
  </head>
  <body>
    <div class="preloader">
      <div class="wrapper-triangle">
        <div class="pen">
            <img src="img/web/carga.png" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">    
          <div class="line-triangle">
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
          </div>
          <div class="line-triangle">
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
          </div>
          <div class="line-triangle">
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="page">
      <!-- Top Banner-->
      <!-- Page Header-->
      <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-modern" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="56px" data-xl-stick-up-offset="56px" data-xxl-stick-up-offset="56px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-inner-outer">
              <div class="rd-navbar-inner">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                    <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap">
                      <span></span>
                    </button>
                  <!-- RD Navbar Brand-->
                    <div class="rd-navbar-brand">
                        <a class="brand" href="{{ Route('home') }}">
                            <img class="brand-logo-dark" src="{{ ('img/web/logo.png') }}" alt="" width="198" height="66"/>
                        </a>
                    </div>
                </div>
                <div class="rd-navbar-right rd-navbar-nav-wrap">
                  <div class="rd-navbar-aside">
                    <ul class="rd-navbar-contacts-2">
                      <li>
                        <div class="unit unit-spacing-xs">
                          <div class="unit-left">
                              <span class="icon mdi mdi-phone"></span>
                            </div>
                            <div class="unit-body">
                              <a class="phone" href="tel:#">944646619</a>
                            </div>
                        </div>
                      </li>
                      <li>
                        <div class="unit unit-spacing-xs">
                            <div class="unit-left">
                              <span class="icon mdi mdi-map-marker"></span>
                            </div>
                            <div class="unit-body">
                                <a class="address" href="#">Ciudad de Lima</a>
                            </div>
                        </div>
                      </li>
                    </ul>
                    <ul class="list-share-2">
                        <li>
                          <a class="icon mdi mdi-facebook" href="#"></a>
                        </li>
                        <li>
                            <a class="icon mdi mdi-twitter" href="#"></a>
                        </li>
                        <li>
                            <a class="icon mdi mdi-instagram" href="#"></a>
                        </li>
                        <li>
                            <a class="icon mdi mdi-google-plus" href="#"></a>
                        </li>
                    </ul>
                  </div>
                  <div class="rd-navbar-main">
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                        <li class="rd-nav-item">
                            <a class="rd-nav-link" href="{{ Route('home') }}">Inicio</a>
                        </li>
                        <li class="rd-nav-item active">
                            <a class="rd-nav-link" href="{{ Route('nosotros') }}">Nosotros</a>
                        </li>
                        <li class="rd-nav-item">
                            <a class="rd-nav-link" href="{{ Route('blog') }}">Blog</a>
                        </li>
                        <li class="rd-nav-item">
                            <a class="rd-nav-link" href="{{ Route('preguntas') }}">Preguntas</a>
                        </li>
                        <li class="rd-nav-item">
                            <a class="rd-nav-link" href="{{ route('login') }}">Ingresar</a>
                        </li>
                    </ul>
                  </div>
                </div>
                <div class="rd-navbar-project-hamburger rd-navbar-project-hamburger-open rd-navbar-fixed-element-1" data-multitoggle=".rd-navbar-inner" data-multitoggle-blur=".rd-navbar-wrap" data-multitoggle-isolate="data-multitoggle-isolate">
                    <div class="project-hamburger">
                        <span class="project-hamburger-arrow"></span>
                        <span class="project-hamburger-arrow"></span>
                        <span class="project-hamburger-arrow"></span>
                    </div>
                </div>
                <div class="rd-navbar-project">
                    <div class="rd-navbar-project-header">
                      <h5 class="rd-navbar-project-title">Tiendas</h5>
                      <div class="rd-navbar-project-hamburger rd-navbar-project-hamburger-close" data-multitoggle=".rd-navbar-inner" data-multitoggle-blur=".rd-navbar-wrap" data-multitoggle-isolate="data-multitoggle-isolate">
                          <div class="project-close">
                              <span></span>
                              <span></span>
                          </div>
                      </div>
                    </div>
                    <div class="rd-navbar-project-content rd-navbar-content">
                      <div>
                        <div class="row gutters-20" data-lightgallery="group">
                          @foreach ($tienda as $t)
                          <div class="col-6">
                            <!-- Thumbnail Creative-->
                            <article class="thumbnail thumbnail-creative">
                              <a href="{{ $t->foto }}" data-lightgallery="item">
                                <div class="thumbnail-creative-figure">
                                    <img src="{{ $t->foto }}" alt="" width="195" height="164"/>
                                </div>
                                <div class="thumbnail-creative-caption">
                                    <span class="icon thumbnail-creative-icon linearicons-magnifier"></span>
                                </div>
                              </a>
                          </article>    
                          </div>
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <!-- parallax top-->
      <!-- Breadcrumbs -->
      <section class="bg-gray-7">
        <div class="breadcrumbs-custom box-transform-wrap context-dark">
          <div class="container">
            <h3 class="breadcrumbs-custom-title">Nosotros</h3>
            <div class="breadcrumbs-custom-decor"></div>
          </div>
          <div class="box-transform" style="background-image: url(web/images/bg-1.jpg);"></div>
        </div>
        <div class="container">
          <ul class="breadcrumbs-custom-path">
            <li><a href="{{ Route('home') }}">Inicio</a></li>
            <li class="active">Nosotros</li>
          </ul>
        </div>
      </section>
      <section class="section section-lg bg-default">
        <div class="container">
          <div class="tabs-custom row row-50 justify-content-center flex-lg-row-reverse text-center text-md-left" id="tabs-4">
            <div class="col-lg-4 col-xl-3">
              <h5 class="text-spacing-200 text-capitalize">1 año de experiencia</h5>
              <ul class="nav list-category list-category-down-md-inline-block">
                <li class="list-category-item wow fadeInRight" role="presentation" data-wow-delay="0s">
                    <a class="active" href="#tabs-nosotros" data-toggle="tab">Nosotros</a>
                </li>
                <li class="list-category-item wow fadeInRight" role="presentation" data-wow-delay=".1s">
                    <a href="#tabs-mision" data-toggle="tab">Misión</a>
                </li>
                <li class="list-category-item wow fadeInRight" role="presentation" data-wow-delay=".2s">
                    <a href="#tabs-vision" data-toggle="tab">Visión</a>
                </li>
                <li class="list-category-item wow fadeInRight" role="presentation" data-wow-delay=".3s">
                    <a href="#tabs-ofrecemos" data-toggle="tab">Lo que ofrecemos</a>
                </li>
                <li class="list-category-item wow fadeInRight" role="presentation" data-wow-delay=".4s">
                    <a href="#tabs-elegirnos" data-toggle="tab">Por que elegirnos</a>
                </li>
              </ul>
              <a class="button button-xl button-primary button-winona" href="contacts.html">Contactenos</a>
            </div>
            <div class="col-lg-8 col-xl-9">
              <!-- Tab panes-->
              <div class="tab-content tab-content-1">
                <div class="tab-pane fade show active" id="tabs-nosotros">
                  <h4>Lo hacemos por ti!</h4>
                  <p>Somos la primera Startup de asistencias personalizadas del mundo, la cual tiene como propósito facilitar la vida de nuestros clientes a través de nuestros disruptivos servicios, las mejores estrategias del comercio electrónico y de marketing digital.</p>
                  <img src="web/images/about-1-835x418.jpg" alt="" width="835" height="418"/>
                </div>
                <div class="tab-pane fade" id="tabs-mision">
                  <h4>Misión</h4>
                  <p>Ingresar Mision</p>
                  <img src="web/images/about-1-835x418.jpg" alt="" width="835" height="418"/>
                </div>
                <div class="tab-pane fade" id="tabs-vision">
                  <h4>Visión</h4>
                  <p>Ingresar Vision.</p>
                  <img src="web/images/about-1-835x418.jpg" alt="" width="835" height="418"/>
                </div>
                <div class="tab-pane fade" id="tabs-ofrecemos">
                    <h4>Lo que ofrecemos</h4>
                    <p>Plataforma, Un sitio web para promocionar & vender sus productos o servicios, gestionar datos y estadísticas de su negocio a través de nuestra marketplace y aplicativo móvil.
                    </p>
                    <p>Estrategia, Social media, fotos, videos y diseños de post para las campañas de marketing digital de sus productos o servicios.
                    </p>
                    <p>Logística, Un equipo altamente cualificado para la realización de cada uno de los servicios de delivery o logística de última milla que demanden sus clientes consumidores.
                    </p>
                    <img src="web/images/about-1-835x418.jpg" alt="" width="835" height="418"/>
                </div>
                <div class="tab-pane fade" id="tabs-elegirnos">
                    <h4>¿Por qué elegirnos?</h4>
                    <p>Porqué sabemos que la seguridad es muy importante, por ende, cada uno de nuestros miembros de equipo se encuentran totalmente uniformados y cuidadosamente seleccionados por rigurosos procesos de filtración.
                    </p>
                    <p>Porqué somos la única empresa que lo desea capacitar de forma mensual para una óptima gestión y operación de su negocio, para así crecer y aprender juntos.
                    </p>
                    <p>Porqué brindamos confianza, rapidez y cuidado en cada uno de los servicios que realizamos para sus clientes consumidores.
                    </p>
                    <p>Porqué sabemos que el emprender un negocio conlleva mucho esfuerzo y sacrificio, por ello nuestra marketplace es 100% gratuita para todos los emprendedores que deseen digitalizar sus negocios de productos o servicios.
                    </p>
                    <img src="web/images/about-1-835x418.jpg" alt="" width="835" height="418"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Icon Classic-->
      <section class="section section-lg bg-gray-100">
        <div class="container">
          <div class="row row-md row-50">
            <div class="col-sm-6 col-xl-4 wow fadeInUp" data-wow-delay="0s">
              <article class="box-icon-classic">
                <div class="unit unit-spacing-lg flex-column text-center flex-md-row text-md-left">
                  <div class="unit-left">
                    <div class="box-icon-classic-icon linearicons-helicopter"></div>
                  </div>
                  <div class="unit-body">
                    <h5 class="box-icon-classic-title">
                        <a href="#">Productividad</a>
                    </h5>
                    <p class="box-icon-classic-text">
                        Mejorar la productividad del negocio de nuestros
                        clientes a través del ahorro de tiempo y costos
                        gracias a la automatización de los procesos
                        administrativos y operativos que brinda nuestro
                        marketplace y aplicativo móvil.
                    </p>
                  </div>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-xl-4 wow fadeInUp" data-wow-delay=".1s">
              <article class="box-icon-classic">
                <div class="unit unit-spacing-lg flex-column text-center flex-md-row text-md-left">
                  <div class="unit-left">
                    <div class="box-icon-classic-icon linearicons-pizza"></div>
                  </div>
                  <div class="unit-body">
                    <h5 class="box-icon-classic-title">
                        <a href="#">Analisis de Clientes</a>
                    </h5>
                    <p class="box-icon-classic-text">
                        Aumentar la capacidad de análisis gracias a la
                        implantación de nuevos sistemas de recogida de
                        datos de clientes (leads).
                    </p>
                  </div>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-xl-4 wow fadeInUp" data-wow-delay=".2s">
              <article class="box-icon-classic">
                <div class="unit unit-spacing-lg flex-column text-center flex-md-row text-md-left">
                  <div class="unit-left">
                    <div class="box-icon-classic-icon linearicons-leaf"></div>
                  </div>
                  <div class="unit-body">
                    <h5 class="box-icon-classic-title">
                        <a href="#">Oportunidades de Negocio</a>
                    </h5>
                    <p class="box-icon-classic-text">
                        Crear nuevas oportunidades de negocios y expandir
                        nacional e internacionalmente las marcas de
                        nuestros clientes.
                    </p>
                  </div>
                </div>
              </article>
            </div>
          </div>
        </div>
      </section>

      <!-- Our Team-->
      <section class="section section-lg section-bottom-md-70 bg-default">
        <div class="container">
            <h3 class="oh">
                <span class="d-inline-block wow slideInUp" data-wow-delay="0s">Nuestro Equipo</span>
            </h3>
          <div class="row row-lg row-40 justify-content-center">
              @foreach ($equipo as $e)
              <div class="col-sm-6 col-lg-3 wow fadeInLeft" data-wow-delay=".2s" data-wow-duration="1s">
                <!-- Team Modern-->
                <article class="team-modern">
                      <a class="team-modern-figure" href="#">
                          <img src="{{ $e->foto }}" alt="" width="270" height="236"/>
                      </a>
                  <div class="team-modern-caption">
                    <h6 class="team-modern-name"><a href="#">{{ $e->nombre }} {{ $e->apellido }}</a></h6>
                    <div class="team-modern-status">{{ $e->tipousuario }}</div>
                    <ul class="list-inline team-modern-social-list">
                      <li><a class="icon mdi mdi-facebook" href="#"></a></li>
                      <li><a class="icon mdi mdi-twitter" href="#"></a></li>
                      <li><a class="icon mdi mdi-instagram" href="#"></a></li>
                      <li><a class="icon mdi mdi-google-plus" href="#"></a></li>
                    </ul>
                  </div>
                </article>
            </div>
            @endforeach
          </div>
        </div>
      </section>
      <section class="section section-lg bg-gray-100 text-left section-relative">
        <div class="container">
          <div class="row row-60 justify-content-center justify-content-xxl-between">
            <div class="col-lg-6 col-xxl-5 position-static">
              <h3>Nuestra Historia</h3>
              <div class="tabs-custom" id="tabs-5">
                <div class="tab-content tab-content-1">
                  <div class="tab-pane fade show active" id="tabs-2021">
                    <h5 class="font-weight-normal text-transform-none text-spacing-75">Nacimiento qFacil.</p>
                  </div>
                </div>
                <div class="list-history-wrap">
                  <ul class="nav list-history">
                    <li class="list-history-item" role="presentation">
                        <a class="active" href="#tabs-2021" data-toggle="tab">
                            <div class="list-history-circle"></div>
                            2021
                        </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-9 col-lg-6 position-static index-1">
              <div class="bg-image-right-1 bg-image-right-lg">
                  <img src="web/images/our_history-1110x710.jpg" alt="" width="1110" height="710"/>
                <div class="link-play-modern">
                    <a class="icon mdi mdi-play" data-lightgallery="item" href="https://www.youtube.com/watch?v=QwUBNSKjCdU"></a>
                  <div class="link-play-modern-title">Como<span>Trabajamos</span></div>
                  <div class="link-play-modern-decor"></div>
                </div>
                <div class="box-transform" style="background-image: url(web/images/our_history-1110x710.jpg);"></div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Our clients-->
      <section class="section section-lg bg-default text-md-left">
        <div class="container">
          <div class="row row-60 justify-content-center flex-lg-row-reverse">
            <div class="col-md-8 col-lg-6 col-xl-5">
              <div class="offset-left-xl-70">
                <h3 class="heading-3">Lo que opina la gente</h3>
                <div class="slick-quote">
                  <!-- Slick Carousel-->
                  <div class="slick-slider carousel-parent" data-autoplay="true" data-swipe="true" data-items="1" data-child="#child-carousel-5" data-for="#child-carousel-5" data-slide-effect="true">
                    @foreach ($comentario as $c)
                    <div class="item">
                        <!-- Quote Modern-->
                        <article class="quote-modern">
                            <h5 class="quote-modern-text">
                              <span class="q">{{ $c->comentario }}</span>
                            </h5>
                            <h5 class="quote-modern-author">{{ $c->usuario }},</h5>
                            <p class="quote-modern-status">{{ $c->titulo }}</p>
                        </article>
                      </div>    
                    @endforeach
                  </div>
                  <div class="slick-slider child-carousel" id="child-carousel-5" data-arrows="true" data-for=".carousel-parent" data-items="4" data-sm-items="4" data-md-items="4" data-lg-items="4" data-xl-items="4" data-slide-to-scroll="1">
                      @foreach ($comentario as $c)
                        <div class="item">
                            <img class="img-circle" src="{{ $c->foto }}" alt="" width="83" height="83"/>
                        </div>      
                      @endforeach
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-xl-7">
                <img src="web/images/wp-say-669x447.jpg" alt="" width="669" height="447"/>
            </div>
          </div>
        </div>
      </section>

      <!-- Bottom Banner--><a class="section section-banner" href="https://www.templatemonster.com/intense-multipurpose-html-template.html" style="background-image: url(images/banner/background-03-1920x310.jpg); background-image: -webkit-image-set( url(images/banner/background-03-1920x310.jpg) 1x, url(images/banner/background-03-3840x620.jpg) 2x )"><img src="images/banner/foreground-03-1600x310.png" srcset="images/banner/foreground-03-1600x310.png 1x, images/banner/foreground-03-3200x620.png 2x" alt="" width="1600" height="310"></a>
      <!-- Page Footer-->
      <footer class="section footer-modern context-dark footer-modern-2">
        <div class="footer-modern-line">
          <div class="container">
            <div class="row row-50">
              <div class="col-md-6 col-lg-4">
                <h5 class="footer-modern-title oh-desktop"><span class="d-inline-block wow slideInLeft">TIENDAS</span></h5>
                <ul class="footer-modern-list d-inline-block d-sm-block wow fadeInUp">
                    @foreach ($tienda as $t)
                        <li><a href="#">{{ $t->nombre }}</a></li>      
                    @endforeach
                </ul>
              </div>
              <div class="col-md-6 col-lg-4 col-xl-3">
                <h5 class="footer-modern-title oh-desktop">
                    <span class="d-inline-block wow slideInLeft">Información</span>
                </h5>
                <ul class="footer-modern-list d-inline-block d-sm-block wow fadeInUp">
                    <li><a href="{{ Route('nosotros') }}">Nosotros</a></li>
                    <li><a href="{{ Route('blog') }}">Blog</a></li>
                    <li><a href="{{ Route('preguntas') }}">Preguntas</a></li>
                </ul>
              </div>
              <div class="col-lg-4 col-xl-5">
                <h5 class="footer-modern-title oh-desktop"><span class="d-inline-block wow slideInLeft">Noticias</span></h5>
                <p class="wow fadeInRight">Registre su correo para recibir promociones.</p>
                <!-- RD Mailform-->
                <form class="rd-form rd-mailform rd-form-inline rd-form-inline-sm oh-desktop" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                  <div class="form-wrap wow slideInUp">
                    <input class="form-input" id="subscribe-form-2-email" type="email" name="email" data-constraints="@Email @Required"/>
                    <label class="form-label" for="subscribe-form-2-email">Ingrese su correo</label>
                  </div>
                  <div class="form-button form-button-2 wow slideInRight">
                    <button class="button button-sm button-icon-3 button-primary button-winona" type="submit"><span class="d-none d-xl-inline-block">ENVIAR</span><span class="icon mdi mdi-telegram d-xl-none"></span></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-modern-line-2">
          <div class="container">
            <div class="row row-30 align-items-center">
              <div class="col-sm-6 col-md-7 col-lg-4 col-xl-4">
                <div class="row row-30 align-items-center text-lg-center">
                    <div class="col-md-7 col-xl-6">
                      <a class="brand" href="{{Route('home')}}">
                          <img src="{{ ('img/web/LogoFooter.png') }}" alt="" width="198" height="66"/>
                        </a>
                    </div>
                  <div class="col-md-5 col-xl-6">
                    <div class="iso-1"><span><img src="{{ ('img\web\playstore.png') }}" alt="" width="58" height="25"/></span><span class="iso-1-big"><img src="{{ ('img\web\appstore.png') }}" alt="" width="58" height="25"/></span></div>
                    
                  </div>
                  
                </div>
              </div>
              <div class="col-sm-6 col-md-12 col-lg-8 col-xl-8 oh-desktop">
                <div class="group-xmd group-sm-justify">
                  <div class="footer-modern-contacts wow slideInUp">
                    <div class="unit unit-spacing-sm align-items-center">
                      <div class="unit-left"><span class="icon icon-24 mdi mdi-phone"></span></div>
                      <div class="unit-body"><a class="phone" href="tel:#">944646619</a></div>
                    </div>
                  </div>
                  <div class="footer-modern-contacts wow slideInDown">
                    <div class="unit unit-spacing-sm align-items-center">
                      <div class="unit-left"><span class="icon mdi mdi-email"></span></div>
                      <div class="unit-body"><a class="mail" href="mailto:#">info@qfacil.com</a></div>
                    </div>
                  </div>
                  <div class="wow slideInRight">
                    <ul class="list-inline footer-social-list footer-social-list-2 footer-social-list-3">
                      <li><a class="icon mdi mdi-facebook" href="#"></a></li>
                      <li><a class="icon mdi mdi-twitter" href="#"></a></li>
                      <li><a class="icon mdi mdi-instagram" href="#"></a></li>
                      <li><a class="icon mdi mdi-google-plus" href="#"></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-modern-line-3">
          <div class="container">
            <div class="row row-10 justify-content-between">
              <div class="col-md-6"><span>Ciudad de Lima</span></div>
              <div class="col-md-auto">
                <!-- Rights-->
                <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span></span><span>.&nbsp;</span><span>All Rights Reserved.</span><span> Design&nbsp;by&nbsp;<a href="">InfoRad</a></span></p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src="{{('web/js/core.min.js')}}"></script>
    <script src="{{ ('web/js/script.js') }}"></script>
  </body>
</html>