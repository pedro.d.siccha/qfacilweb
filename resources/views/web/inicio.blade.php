<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>QFácil</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="{{ ('img/web/icono.ico') }}" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:100,300,300i,400,500,600,700,900%7CRaleway:500">
    <link rel="stylesheet" href="{{ ('web/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ ('web/css/fonts.css') }}">
    <link rel="stylesheet" href="{{ ('web/css/style.css') }}">
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;">
        <a href="http://windows.microsoft.com/en-US/internet-explorer/">
            <img src="web/images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">
        </a>
    </div>
    <script src="{{ ('web/js/html5shiv.min.js') }}"></script>
    <![endif]-->
  </head>
  <body>
    <div class="preloader">
      <div class="wrapper-triangle">
          
        <div class="pen">
            <img src="img/web/carga.png" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">    
          <div class="line-triangle">
            <div class="triangle">
                
            </div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
          </div>
          <div class="line-triangle">
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
          </div>
          <div class="line-triangle">
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
            <div class="triangle"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="page">
      <!-- Top Banner-->
        <a class="section section-banner text-center d-none d-xl-block" href="https://www.templatemonster.com/intense-multipurpose-html-template.html" style="background-image: url(web/images/banner/background-04-1920x60.jpg); background-image: -webkit-image-set( url(web/images/banner/background-04-1920x60.jpg) 1x, url(web/images/banner/background-04-3840x120.jpg) 2x )">
           <!-- <img src="{{ ('web/images/banner/foreground-04-1600x60.png') }}" srcset="{{ ('web/images/banner/foreground-04-1600x60.png') }} 1x, web/images/banner/foreground-04-3200x120.png 2x" alt="" width="1600" height="310"> -->
        </a>
      <!-- Page Header-->
      <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-modern" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="56px" data-xl-stick-up-offset="56px" data-xxl-stick-up-offset="56px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-inner-outer">
              <div class="rd-navbar-inner">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <!-- RD Navbar Brand-->
                  <div class="rd-navbar-brand">
                      <a class="brand" href="index.html">
                          <img class="brand-logo-dark" src="{{ ('img/web/logo.png') }}" alt="" width="198" height="66"/>
                        </a>
                    </div>
                </div>
                <div class="rd-navbar-right rd-navbar-nav-wrap">
                  <div class="rd-navbar-aside">
                    <ul class="rd-navbar-contacts-2">
                      <li>
                        <div class="unit unit-spacing-xs">
                            <div class="unit-left">
                              <span class="icon mdi mdi-phone"></span>
                            </div>
                            <div class="unit-body">
                              <a class="phone" href="tel:#">944646619</a>
                            </div>
                        </div>
                      </li>
                      <li>
                        <div class="unit unit-spacing-xs">
                            <div class="unit-left">
                              <span class="icon mdi mdi-map-marker"></span>
                            </div>
                            <div class="unit-body">
                                <a class="address" href="#">514 S. Magnolia St. Orlando, FL 32806</a>
                            </div>
                        </div>
                      </li>
                    </ul>
                    <ul class="list-share-2">
                      <li><a class="icon mdi mdi-facebook" href="#"></a></li>
                      <li><a class="icon mdi mdi-twitter" href="#"></a></li>
                      <li><a class="icon mdi mdi-instagram" href="#"></a></li>
                      <li><a class="icon mdi mdi-google-plus" href="#"></a></li>
                    </ul>
                  </div>
                  <div class="rd-navbar-main">
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                      <li class="rd-nav-item active"><a class="rd-nav-link" href="{{ Route('home') }}">Inicio</a>
                      </li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="{{ Route('nosotros') }}">Nosotros</a>
                      </li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="{{ Route('blog') }}">Blog</a>
                      </li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="{{ Route('preguntas') }}">Preguntas</a>
                      </li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="{{ route('login') }}">Ingresar</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="rd-navbar-project-hamburger rd-navbar-project-hamburger-open rd-navbar-fixed-element-1" data-multitoggle=".rd-navbar-inner" data-multitoggle-blur=".rd-navbar-wrap" data-multitoggle-isolate="data-multitoggle-isolate">
                  <div class="project-hamburger">
                      <span class="project-hamburger-arrow"></span>
                      <span class="project-hamburger-arrow"></span>
                      <span class="project-hamburger-arrow"></span>
                  </div>
                </div>
                <div class="rd-navbar-project">
                  <div class="rd-navbar-project-header">
                    <h5 class="rd-navbar-project-title">Tiendas</h5>
                    <div class="rd-navbar-project-hamburger rd-navbar-project-hamburger-close" data-multitoggle=".rd-navbar-inner" data-multitoggle-blur=".rd-navbar-wrap" data-multitoggle-isolate="data-multitoggle-isolate">
                        <div class="project-close">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                  </div>
                  <div class="rd-navbar-project-content rd-navbar-content">
                    <div>
                      <div class="row gutters-20" data-lightgallery="group">
                        @foreach ($tienda as $t)
                        <div class="col-6">
                          <!-- Thumbnail Creative-->
                          <article class="thumbnail thumbnail-creative">
                            <a href="{{ $t->foto }}" data-lightgallery="item">
                              <div class="thumbnail-creative-figure">
                                  <img src="{{ $t->foto }}" alt="" width="195" height="164"/>
                              </div>
                              <div class="thumbnail-creative-caption">
                                  <span class="icon thumbnail-creative-icon linearicons-magnifier"></span>
                              </div>
                            </a>
                        </article>    
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <!-- Swiper-->
      <section class="section swiper-container swiper-slider swiper-slider-2 swiper-slider-3" data-loop="true" data-autoplay="5000" data-simulate-touch="false" data-slide-effect="fade">
        <div class="swiper-wrapper text-sm-left">
            @foreach ($banner as $b)
            <div class="swiper-slide context-dark" data-slide-bg="{{ $b->url }}">
                <div class="swiper-slide-caption section-md">
                  <div class="container">
                    <div class="row">
                      <div class="col-sm-9 col-md-8 col-lg-7 col-xl-7 offset-lg-1 offset-xxl-0">
                        <h1 class="oh swiper-title">
                            <span class="d-inline-block" data-caption-animate="slideInUp" data-caption-delay="0">{{ $b->titulo }}</span>
                        </h1>
                        <p class="big swiper-text" data-caption-animate="fadeInLeft" data-caption-delay="300">{{ $b->descripcion }}</p>
                        <a class="button button-lg button-primary button-winona button-shadow-2" href="#" data-caption-animate="fadeInUp" data-caption-delay="300">S/. {{ $b->costo }}</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>      
            @endforeach
        
        </div>
        <!-- Swiper Pagination-->
        <div class="swiper-pagination" data-bullet-custom="true"></div>
        <!-- Swiper Navigation-->
        <div class="swiper-button-prev">
          <div class="preview">
            <div class="preview__img"></div>
          </div>
          <div class="swiper-button-arrow"></div>
        </div>
        <div class="swiper-button-next">
          <div class="swiper-button-arrow"></div>
          <div class="preview">
            <div class="preview__img"></div>
          </div>
        </div>
      </section>
      <!-- What We Offer-->
      <section class="section section-md bg-default">
        <div class="container">
          <h3 class="oh-desktop"><span class="d-inline-block wow slideInDown">Nuestros Servicios</span></h3>
          <div class="row row-md row-30">
              @foreach ($servicios as $s)
              <div class="col-sm-6 col-lg-4">
                <div class="oh-desktop">
                  <!-- Services Terri-->
                  <article class="services-terri wow slideInUp">
                    <div class="services-terri-figure">
                        <img src="{{ $s->url }}" alt="" width="370" height="278"/>
                    </div>
                    <div class="services-terri-caption"><span class="services-terri-icon linearicons-steak"></span>
                      <h5 class="services-terri-title"><a href="#">{{ $s->servicio }}</a></h5>
                    </div>
                  </article>
                </div>
              </div>      
              @endforeach
          </div>
        </div>
      </section>

      <!-- Section CTA-->
      <section class="primary-overlay section parallax-container" data-parallax-img="img/web/banner/bannermedio.png">
        <div class="parallax-content section-xl context-dark text-md-left">
          <div class="container">
            <div class="row justify-content-end">
              <div class="col-sm-8 col-md-7 col-lg-5">
                <div class="cta-modern">
                  <h3 class="cta-modern-title wow fadeInRight">El mejor equipo</h3>
                  <p class="lead">Contamos con un exelente equipo, especializado y capacitado para su servicio.</p>
                  <p class="cta-modern-text oh-desktop" data-wow-delay=".1s">
                      <span class="cta-modern-decor wow slideInLeft"></span>
                      <span class="d-inline-block wow slideInDown">Ben Smith, Founder</span>
                  </p>
                  <a class="button button-md button-secondary-2 button-winona wow fadeInUp" href="#" data-wow-delay=".2s">NUESTRO EQUIPO</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Our Shop-->
      <section class="section section-lg bg-default">
        <div class="container">
            <h3 class="oh-desktop">
              <span class="d-inline-block wow slideInUp">Productos Destacados</span>
            </h3>
          <div class="row row-lg row-30">
              @foreach ($producto as $p)
              <div class="col-sm-6 col-lg-4 col-xl-3">
                <!-- Product-->
                <article class="product wow fadeInLeft" data-wow-delay=".15s">
                  <div class="product-figure">
                      <img src="{{ $p->foto }}" alt="" width="161" height="162"/>
                  </div>
                  <div class="product-rating">
                      <span class="mdi mdi-star"></span>
                      <span class="mdi mdi-star"></span>
                      <span class="mdi mdi-star"></span>
                      <span class="mdi mdi-star"></span>
                      <span class="mdi mdi-star text-gray-13"></span>
                  </div>
                  <h6 class="product-title">{{ $p->nombre }}</h6>
                  <div class="product-price-wrap">
                    <div class="product-price">S/. {{ $p->precio }}</div>
                  </div>
                  <div class="product-button">
                      <div class="button-wrap">
                        <a class="button button-xs button-primary button-winona" href="#">Agregar al Carrito</a>
                      </div>
                      <div class="button-wrap">
                          <a class="button button-xs button-secondary button-winona" href="#">Ver {{ $p->nombre }}</a>
                      </div><span class="product-badge product-badge-new">Nuevo</span>
                  </div>
                </article>
              </div>      
              @endforeach
            
          </div>
        </div>
      </section>

      <!-- Section CTA-->
      <section class="primary-overlay section parallax-container" data-parallax-img="{{ ('img/web/banner/unete.jpg') }}">
        <div class="parallax-content section-xxl context-dark text-md-left">
          <div class="container">
            <div class="row justify-content-end">
              <div class="col-sm-9 col-md-7 col-lg-5">
                <div class="cta-modern">
                    <h3 class="cta-modern-title cta-modern-title-2 oh-desktop">
                      <span class="d-inline-block wow fadeInLeft">-Unete a nosotros</span>
                    </h3>
                    <p class="cta-modern-text cta-modern-text-2 oh-desktop" data-wow-delay=".1s">
                      <span class="cta-modern-decor cta-modern-decor-2 wow slideInLeft"></span>
                      <span class="d-inline-block wow slideInUp">Puedes unirte como asociado o como repartidor</span>
                    </p>
                    <a class="button button-lg button-secondary button-winona wow fadeInRight" href="contacts.html" data-wow-delay=".2s">UNIRTE</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- What We Offer-->
      <section class="section section-xl bg-default">
        <div class="container">
          <h3 class="wow fadeInLeft">Que Opina la Gente</h3>
        </div>
        <div class="container container-style-1">
          <div class="owl-carousel owl-style-12" data-items="1" data-sm-items="2" data-lg-items="3" data-margin="30" data-xl-margin="45" data-autoplay="true" data-nav="true" data-center="true" data-smart-speed="400">
            <!-- Quote Tara-->
            @foreach ($comentario as $c)
            <article class="quote-tara">
                <div class="quote-tara-caption">
                  <div class="quote-tara-text">
                    <p class="q">{{ $c->comentario }}</p>
                  </div>
                  <div class="quote-tara-figure">
                      <img src="{{ $c->foto }}" alt="" width="115" height="115"/>
                  </div>
                </div>
                <h6 class="quote-tara-author">{{ $c->titulo }}</h6>
                <div class="quote-tara-status">{{ $c->usuario }}</div>
              </article>    
            @endforeach
            
          </div>
        </div>
      </section>

      <section class="section section-last bg-default">
        <div class="container-fluid container-inset-0 isotope-wrap">
            <div class="container">
                <h3 class="wow fadeInLeft">TIENDAS</h3>
              </div>
          <div class="row row-10 gutters-10 isotope" data-isotope-layout="masonry" data-isotope-group="gallery" data-lightgallery="group">
            @foreach ($tienda as $t)
            <div class="col-xs-6 col-sm-4 col-xl-2 isotope-item oh-desktop">
                <!-- Thumbnail Mary-->
                <article class="thumbnail thumbnail-mary thumbnail-mary-2 wow slideInLeft">
                  <a class="thumbnail-mary-figure" href="{{ $t->foto }}" data-lightgallery="item">
                      <img src="{{ $t->foto }}" alt="" width="310" height="585"/>
                  </a>
                  <div class="thumbnail-mary-caption">
                    <div>
                      <h6 class="thumbnail-mary-title">
                          <a href="#">{{ $t->nombre }}</a>
                      </h6>
                      <div class="thumbnail-mary-location">{{ $t->categoria }}</div>
                    </div>
                  </div>
                </article>
              </div>      
            @endforeach
          </div>
        </div>
      </section>

      <!-- Section Services  Last section-->
      <section class="section section-sm bg-default">
        <div class="container">
          <div class="owl-carousel owl-style-11 dots-style-2" data-items="1" data-sm-items="1" data-lg-items="2" data-xl-items="4" data-margin="30" data-dots="true" data-mouse-drag="true" data-rtl="true">
            <article class="box-icon-megan wow fadeInUp">
              <div class="box-icon-megan-header">
                <div class="box-icon-megan-icon linearicons-bag"></div>
              </div>
                <h5 class="box-icon-megan-title">
                  <a href="#">Compras Personalizadas</a>
                </h5>
              <p class="box-icon-megan-text">Nuestro personal hará las compras que ustede necesita ahorrandole tiempo para que pueda disfrutar su día.</p>
            </article>
            <article class="box-icon-megan wow fadeInUp" data-wow-delay=".05s">
              <div class="box-icon-megan-header">
                <div class="box-icon-megan-icon linearicons-map2"></div>
              </div>
              <h5 class="box-icon-megan-title"><a href="#">Seguimiento GPS</a></h5>
              <p class="box-icon-megan-text">Podrá realizar el seguimiento de su pedido en tiempo real.</p>
            </article>
            <article class="box-icon-megan wow fadeInUp" data-wow-delay=".1s">
              <div class="box-icon-megan-header">
                <div class="box-icon-megan-icon linearicons-radar"></div>
              </div>
              <h5 class="box-icon-megan-title"><a href="#">Conectividad</a></h5>
              <p class="box-icon-megan-text">Contacto directo con su repartidor.</p>
            </article>
            <article class="box-icon-megan wow fadeInUp" data-wow-delay=".15s">
              <div class="box-icon-megan-header">
                <div class="box-icon-megan-icon linearicons-thumbs-up"></div>
              </div>
              <h5 class="box-icon-megan-title"><a href="#">Mejor Servicio</a></h5>
              <p class="box-icon-megan-text">Le brindamos un servicio de primera calidad.</p>
            </article>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      <footer class="section footer-modern context-dark footer-modern-2">
        <div class="footer-modern-line">
          <div class="container">
            <div class="row row-50">
              <div class="col-md-6 col-lg-4">
                <h5 class="footer-modern-title oh-desktop"><span class="d-inline-block wow slideInLeft">TIENDAS</span></h5>
                <ul class="footer-modern-list d-inline-block d-sm-block wow fadeInUp">
                    @foreach ($tienda as $t)
                        <li><a href="#">{{ $t->nombre }}</a></li>      
                    @endforeach
                </ul>
              </div>
              <div class="col-md-6 col-lg-4 col-xl-3">
                <h5 class="footer-modern-title oh-desktop">
                    <span class="d-inline-block wow slideInLeft">Información</span>
                </h5>
                <ul class="footer-modern-list d-inline-block d-sm-block wow fadeInUp">
                  <li><a href="{{ Route('nosotros') }}">Nosotros</a></li>
                  <li><a href="{{ Route('blog') }}">Blog</a></li>
                  <li><a href="{{ Route('preguntas') }}">Preguntas</a></li>
                </ul>
              </div>
              <div class="col-lg-4 col-xl-5">
                <h5 class="footer-modern-title oh-desktop"><span class="d-inline-block wow slideInLeft">Noticias</span></h5>
                <p class="wow fadeInRight">Registre su correo para recibir promociones.</p>
                <!-- RD Mailform-->
                <form class="rd-form rd-mailform rd-form-inline rd-form-inline-sm oh-desktop" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                  <div class="form-wrap wow slideInUp">
                    <input class="form-input" id="subscribe-form-2-email" type="email" name="email" data-constraints="@Email @Required"/>
                    <label class="form-label" for="subscribe-form-2-email">Ingrese su correo</label>
                  </div>
                  <div class="form-button form-button-2 wow slideInRight">
                    <button class="button button-sm button-icon-3 button-primary button-winona" type="submit"><span class="d-none d-xl-inline-block">ENVIAR</span><span class="icon mdi mdi-telegram d-xl-none"></span></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-modern-line-2">
          <div class="container">
            <div class="row row-30 align-items-center">
              <div class="col-sm-6 col-md-7 col-lg-4 col-xl-4">
                <div class="row row-30 align-items-center text-lg-center">
                    <div class="col-md-7 col-xl-6">
                      <a class="brand" href="{{Route('home')}}">
                          <img src="{{ ('img/web/LogoFooter.png') }}" alt="" width="198" height="66"/>
                        </a>
                    </div>
                  <div class="col-md-5 col-xl-6">
                    <div class="iso-1"><span><img src="{{ ('img\web\playstore.png') }}" alt="" width="58" height="25"/></span><span class="iso-1-big"><img src="{{ ('img\web\appstore.png') }}" alt="" width="58" height="25"/></span></div>
                    
                  </div>
                  
                </div>
              </div>
              <div class="col-sm-6 col-md-12 col-lg-8 col-xl-8 oh-desktop">
                <div class="group-xmd group-sm-justify">
                  <div class="footer-modern-contacts wow slideInUp">
                    <div class="unit unit-spacing-sm align-items-center">
                      <div class="unit-left"><span class="icon icon-24 mdi mdi-phone"></span></div>
                      <div class="unit-body"><a class="phone" href="tel:#">944646619</a></div>
                    </div>
                  </div>
                  <div class="footer-modern-contacts wow slideInDown">
                    <div class="unit unit-spacing-sm align-items-center">
                      <div class="unit-left"><span class="icon mdi mdi-email"></span></div>
                      <div class="unit-body"><a class="mail" href="mailto:#">info@qfacil.com</a></div>
                    </div>
                  </div>
                  <div class="wow slideInRight">
                    <ul class="list-inline footer-social-list footer-social-list-2 footer-social-list-3">
                      <li><a class="icon mdi mdi-facebook" href="#"></a></li>
                      <li><a class="icon mdi mdi-twitter" href="#"></a></li>
                      <li><a class="icon mdi mdi-instagram" href="#"></a></li>
                      <li><a class="icon mdi mdi-google-plus" href="#"></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-modern-line-3">
          <div class="container">
            <div class="row row-10 justify-content-between">
              <div class="col-md-6"><span>Ciudad de Lima</span></div>
              <div class="col-md-auto">
                <!-- Rights-->
                <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span></span><span>.&nbsp;</span><span>All Rights Reserved.</span><span> Design&nbsp;by&nbsp;<a href="">InfoRad</a></span></p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src="{{ ('web/js/core.min.js') }}"></script>
    <script src="{{ ('web/js/script.js') }}"></script>
    <!-- coded by Himic-->
  </body>
</html>