<!DOCTYPE html>
<html lang="es"> 
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link rel="shortcut icon" href="img/web/logo.png">
<title>QFacil Perú</title>
   
   <link rel='dns-prefetch' href='web/web/fonts.googleapis.com/index.html' />
   <link rel='dns-prefetch' href='web/web/s.w.org/index.html' />
   <link rel="alternate" type="application/rss+xml" title="QFacil Perú &raquo; Feed" href="web/feed/index.html" />
   <link rel="alternate" type="application/rss+xml" title="QFacil Perú &raquo; Feed de los comentarios" href="web/comments/feed/index.html" />
   
	<style type="text/css">
      img.wp-smiley,
      img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
      }
   </style>
	<link rel='stylesheet' id='wp-block-library-css'  href='{{ ('web/wp-includes/css/dist/block-library/style.mine23c.css?ver=5.7') }}' type='text/css' media='all' />
   <link rel='stylesheet' id='wc-block-vendors-style-css'  href='{{("web/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-stylecb20.css?ver=4.4.3")}}' type='text/css' media='all' />
   <link rel='stylesheet' id='wc-block-style-css'  href='{{ ("web/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/stylecb20.css?ver=4.4.3") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='wpc-block-style-css-css'  href='{{ ("web/wp-content/plugins/wp-cafe/core/guten-block/dist/blocks.style.build.css") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='contact-form-7-css'  href='{{ ("web/wp-content/plugins/contact-form-7/includes/css/styles91d5.css?ver=5.4") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='rs-plugin-settings-css'  href='{{ ("web/wp-content/plugins/revslider/public/assets/css/rs6fab5.css?ver=6.3.8") }}' type='text/css' media='all' />
   <style id='rs-plugin-settings-inline-css' type='text/css'>
      #rs-demo-id {}
   </style>
   <link rel='stylesheet' id='woocommerce-layout-css'  href='{{ ("web/wp-content/plugins/woocommerce/assets/css/woocommerce-layout0e7d.css?ver=5.1.0") }}' type='text/css' media='all' />
   <style id='woocommerce-layout-inline-css' type='text/css'>
      .infinite-scroll .woocommerce-pagination {
         display: none;
      }
   </style>
   <link rel='stylesheet' id='woocommerce-smallscreen-css'  href='{{ ("web/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen0e7d.css?ver=5.1.0") }}' type='text/css' media='only screen and (max-width: 768px)' />
   <link rel='stylesheet' id='woocommerce-general-css'  href='{{ ("web/wp-content/plugins/woocommerce/assets/css/woocommerce0e7d.css?ver=5.1.0") }}' type='text/css' media='all' />
   <style id='woocommerce-inline-inline-css' type='text/css'>
      .woocommerce form .form-row .required { visibility: visible; }
   </style>
   <link rel='stylesheet' id='wur_content_css-css'  href='{{("web/wp-content/plugins/wp-ultimate-review/assets/public/css/content-pagee23c.css?ver=5.7")}}' type='text/css' media='all' />
   <link rel='stylesheet' id='dashicons-css'  href='{{ ("web/wp-includes/css/dashicons.mine23c.css?ver=5.7") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='flatpicker-css'  href='{{ ("web/wp-content/plugins/wp-cafe/assets/css/flatpickr.mine7f0.css?ver=1.3.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='jquery-timepicker-css'  href='{{ ("web/wp-content/plugins/wp-cafe/assets/css/jquery.timepicker.mine7f0.css?ver=1.3.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='wpc-icon-css'  href='{{ ("web/wp-content/plugins/wp-cafe/assets/css/wpc-icone7f0.css?ver=1.3.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='wpc-public-css'  href='{{ ("web/wp-content/plugins/wp-cafe/assets/css/wpc-publice7f0.css?ver=1.3.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementskit-parallax-style-css'  href='{{ ("web/wp-content/themes/gloreya/core/parallax/assets/css/style7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-icons-ekiticons-css'  href='{{ ("web/wp-content/plugins/elementskit-lite/modules/controls/assets/css/ekiticonsad76.css?ver=5.9.0") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-icons-css'  href='{{ ("web/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min21f9.css?ver=5.11.0") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-animations-css'  href='{{ ("web/wp-content/plugins/elementor/assets/lib/animations/animations.minaeb9.css?ver=3.1.4") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-frontend-legacy-css'  href='{{ ("web/wp-content/plugins/elementor/assets/css/frontend-legacy.minaeb9.css?ver=3.1.4") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-frontend-css'  href='{{ ("web/wp-content/plugins/elementor/assets/css/frontend.minaeb9.css?ver=3.1.4") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-post-1489-css'  href='{{ ("web/wp-content/uploads/elementor/css/post-14897072.css?ver=1616104051") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementskit-css-widgetarea-control-editor-css'  href='{{ ("web/wp-content/plugins/elementskit-lite/modules/controls/assets/css/widgetarea-editor77e6.css?ver=2.2.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-global-css'  href='{{ ("web/wp-content/uploads/elementor/css/global7072.css?ver=1616104051") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-post-579-css'  href='{{ ("web/wp-content/uploads/elementor/css/post-5799708.css?ver=1616115895") }}' type='text/css' media='all' />
   
   <link rel='stylesheet' id='bootstrap-min-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/bootstrap.min7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='font-awesome-5-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/font-awesome7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='iconfont-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/iconfont7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='magnific-popup-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/magnific-popup7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='owl-carousel-min-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/owl.carousel.min7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='gloreya-woocommerce-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/woocommerce7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='gloreya-gutenberg-custom-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/gutenberg-custom7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <link rel='stylesheet' id='gloreya-style-css'  href='{{ ("web/wp-content/themes/gloreya/assets/css/master7406.css?ver=2.0.1") }}' type='text/css' media='all' />
   <style id='gloreya-style-inline-css' type='text/css'>

        body{ font-family:"Roboto";font-size:16px; }

        h1{
            font-family:"Barlow";font-weight:800;
        }
        h2{
            font-family:"Barlow";font-weight:700; 
        }
        h3{ 
            font-family:"Barlow";font-weight:700; 
        }

        h4{ 
            font-family:"Barlow";font-weight:700;
        }      

        a, .post-meta span i, .entry-header .entry-title a:hover, .sidebar ul li a:hover{
            color: #e7272d;
        }

        .entry-header .entry-title a,
        .post .entry-header .entry-title a{
            color: #222;
        }
     
        body{
            background-color: #fff;
        }
      
     
        .single-intro-text .count-number, .sticky.post .meta-featured-post,
        .sidebar .widget .widget-title:before, .pagination li.active a, .pagination li:hover a,
        .pagination li.active a:hover, .pagination li:hover a:hover,
        .sidebar .widget.widget_search .input-group-btn,
        .BackTo, .ticket-btn.btn:hover,
        .woocommerce div.product form.cart .button,
        .btn-primary,
        .BackTo,
        .header-book-btn .btn-primary,
        .header .navbar-container .navbar-light .main-menu > li > a:before,
        .header-transparent:before,
        .header-transparent .header-cart .cart-link a sup,
        .header-transparent .navSidebar-button,
        .owl-carousel .owl-dots .owl-dot.active,
        .testimonial-carousel.owl-carousel.style4 .author-name:after,
        .xs-review-box .xs-review .xs-btn,
        .sidebar .widget-title:before,
        .not-found .input-group-btn,
        .ts-product-slider.owl-carousel .owl-nav .owl-prev:hover, .ts-product-slider.owl-carousel .owl-nav .owl-next:hover,
        .woocommerce ul.products li.product .button,.woocommerce ul.products li.product .added_to_cart,
        .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current,
        .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,.sponsor-web-link a:hover i, .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
        .woocommerce span.onsale,
        .navbar-light .navbar-toggler,
        .ts-contact-form .form-group .btn-submit:hover,
        .woocommerce table.cart td.actions button.button,
        .woocommerce a.button, .woocommerce button.button.alt,
        .faq .elementor-accordion .elementor-accordion-item .elementor-tab-title.elementor-active .elementor-accordion-icon,
        .woocommerce ul.products li.product .added_to_cart:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover,.woocommerce .widget_price_filter .ui-slider .ui-slider-handle{
            background: #e7272d;
        }


        .btn-primary,
        .sidebar .widget.widget_search .form-control:focus,
        .not-found .form-control:focus,
        .ts-contact-form .form-group .btn-submit:hover,
        .woocommerce div.product .woocommerce-tabs ul.tabs li.active{
            border-color: #e7272d;
        }

      

        .copyright .footer-social li a i:hover,
        .copyright .copyright-text a,
        .header .navbar-container .navbar-light .main-menu li a:hover,
        .header .navbar-container .navbar-light .main-menu li.active > a,
        .post .entry-header .entry-title a:hover,
        a:hover,
        .ts-footer .footer-menu li a:hover,
        .tag-lists a:hover, .tagcloud a:hover,
        .post-navigation span:hover, .post-navigation h3:hover,
        #rev_slider_4_1  .gloreya .tp-bullet.selected:after,
        #rev_slider_4_1 .gloreya.tparrows::before,
        .woocommerce ul.products li.product .woocommerce-loop-product__title:hover{
            color: #e7272d;
        }

        .footer-widget p strong a,
        .nav-classic-transparent.header .navbar-container .navbar-light .main-menu li a:hover{
            color: #ffe119;

        }

        .header-book-btn .btn-primary:hover,
        .btn-primary:hover,
        .xs-review-box .xs-review .xs-btn:hover,
        .nav-classic-transparent .header-cart .cart-link a sup{
            background: #ffe119;
        }
        .header-book-btn .btn-primary:hover,
        .btn-primary:hover{
            border-color: #ffe119;
        }

        

        
         .header-book-btn .btn-primary{
            background: #e7272d;
            border-color: #e7272d;
        }
         
         .header-book-btn .btn-primary{
            color: #fff;
        }
         
            .header .navbar-container .navbar-light .main-menu > li > a,
            .header-transparent .header-nav-right-info li{
             font-family:"Barlow";font-size:14px;font-weight:700;
         }

        .ts-footer{
            background-color: #1b1b1b;
            padding-top:80px;
            background-image:url(web/wp-content/uploads/2019/10/footer_bg.png);;
            background-repeat: no-repeat;
            background-size: cover;
        }
        
        

      .copyright .copyright-text{
         color: #aaa;
      }

     
</style>
<link rel='stylesheet' id='ekit-widget-styles-css'  href='{{ ("web/wp-content/plugins/elementskit-lite/widgets/init/assets/css/widget-styles77e6.css?ver=2.2.1") }}' type='text/css' media='all' />
<link rel='stylesheet' id='ekit-responsive-css'  href='{{ ("web/wp-content/plugins/elementskit-lite/widgets/init/assets/css/responsive77e6.css?ver=2.2.1") }}' type='text/css' media='all' />

<script type='text/javascript' src='{{ ("web/wp-includes/js/jquery/jquery.min9d52.js?ver=3.5.1") }}' id='jquery-core-js'></script>
<script type='text/javascript' src='{{ ("web/wp-includes/js/jquery/jquery-migrate.mind617.js?ver=3.3.2") }}' id='jquery-migrate-js'></script>
<script type='text/javascript' src='{{ ("web/wp-content/plugins/revslider/public/assets/js/rbtools.minfab5.js?ver=6.3.8") }}' id='tp-tools-js'></script>
<script type='text/javascript' src='{{("web/wp-content/plugins/revslider/public/assets/js/rs6.minfab5.js?ver=6.3.8")}}' id='revmin-js'></script>
<script type='text/javascript' src='{{ ("web/wp-content/plugins/wp-ultimate-review/assets/public/script/content-pagee23c.js?ver=5.7") }}' id='wur_review_content_script-js'></script>
<script type='text/javascript' src='{{ ("web/wp-content/themes/gloreya/core/parallax/assets/js/jarallax7406.js?ver=2.0.1") }}' id='jarallax-js'></script>
<link rel="https://api.w.org/" href="{{ ('web/wp-json/index.html') }}" />
<link rel="alternate" type="application/json" href="{{ ('web/wp-json/wp/v2/pages/579.json') }}" />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="{{ ('web/xmlrpc0db0.php?rsd') }}" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="{{ ('web/wp-includes/wlwmanifest.xml') }}" /> 
<meta name="generator" content="WordPress 5.7" />
<meta name="generator" content="WooCommerce 5.1.0" />
<link rel="canonical" href="{{ ('index.html') }}" />
<link rel='shortlink' href='{{ ("web/indexc74d.html?p=579") }}' />
<link rel="alternate" type="application/json+oembed" href="{{ ('web/wp-json/oembed/1.0/embed61f2.json?url=https%3A%2F%2Fwww.qfacilperu.com%2Fmenu%2F') }}" />
<link rel="alternate" type="text/xml+oembed" href="{{ ('web/wp-json/oembed/1.0/embedb539?url=https%3A%2F%2Fwww.qfacilperu.com%2Fmenu%2F&amp;format=xml') }}" />

<ul class="wpc_cart_block"><a href="#" class="wpc_cart_icon">
        <i class="wpcafe-cart_icon"></i>
        <sup class="basket-item-count" style="display: inline-block;">
            <span class="cart-items-count count" id="wpc-mini-cart-count">
            </span>
        </sup>
    </a>
    <li class="wpc-menu-mini-cart wpc_background_color">
        <div class="widget_shopping_cart_content">
         
	<p class="woocommerce-mini-cart__empty-message">No hay productos en el carrito.</p>
        </div>
    </li>
</ul>
			<script type="text/javascript">
				var elementskit_module_parallax_url = "{{ ('web/wp-content/themes/gloreya/core/parallax/index.html') }}"
			</script>
			<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta name="generator" content="Powered by Slider Revolution 6.3.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<link rel="icon" href="{{ ('web/wp-content/uploads/2021/03/cropped-QFacil-1-32x32.png') }}" sizes="32x32" />
<link rel="icon" href="{{ ('web/wp-content/uploads/2021/03/cropped-QFacil-1-192x192.png') }}" sizes="192x192" />
<link rel="apple-touch-icon" href="{{ ('web/wp-content/uploads/2021/03/cropped-QFacil-1-180x180.png') }}" />
<meta name="msapplication-TileImage" content="{{ ('https://www.qfacilperu.com/wp-content/uploads/2021/03/cropped-QFacil-1-270x270.png') }}" />
<script type="text/javascript">function setREVStartSize(e){
			//window.requestAnimationFrame(function() {				 
				window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;	
				window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;	
				try {								
					var pw = document.getElementById(e.c).parentNode.offsetWidth,
						newh;
					pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
					e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
					e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
					e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
					e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
					e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
					e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
					e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
					if(e.layout==="fullscreen" || e.l==="fullscreen") 						
						newh = Math.max(e.mh,window.RSIH);					
					else{					
						e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
						for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
						e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
						e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
						for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
											
						var nl = new Array(e.rl.length),
							ix = 0,						
							sl;					
						e.tabw = e.tabhide>=pw ? 0 : e.tabw;
						e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
						e.tabh = e.tabhide>=pw ? 0 : e.tabh;
						e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
						for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
						sl = nl[0];									
						for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
						var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					
						newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
					}				
					if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));					
					document.getElementById(e.c).height = newh+"px";
					window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";				
				} catch(e){
					console.log("Failure at Presize of Slider:" + e)
				}					   
			//});
		  };</script>
		<style type="text/css" id="wp-custom-css">
			
         .menu-block .inner-box .text {
            padding-right: 50px;
         }
         .intro-content-section{
            overflow: hidden;
         }
         .subtitle{
               position: relative;
         }
         .subtitle::after{
               width: 100px;
               height: 2px;
               background: #e7272d;
               right: 0;
               margin-left: 30px;
               content: '';
               display: inline-block;
               vertical-align: middle;
            }		
      </style>
</head>

<body class="page-template page-template-template page-template-full-width-template page-template-templatefull-width-template-php page page-id-579 theme-gloreya woocommerce-no-js sidebar-active sidebar-class elementor-default elementor-kit-1489 elementor-page elementor-page-579">
	
<!-- header nav start-->
   <header id="header" class="header header-standard  navbar-sticky">

   <!-- navbar container start -->
      <div class="navbar-container">
         <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
               <a class="navbar-brand" href="/">
                  <img src="{{ ('img/web/logo.png') }}" alt="QFacil Perú">
               </a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#primary-nav" aria-controls="primary-nav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon">
                     <i class="icon icon-menu"></i>
                  </span>
               </button>   

               <div id="primary-nav" class="collapse navbar-collapse">
                  <ul id="main-menu" class="navbar-nav  main-menu">
                     <li id="menu-item-1020" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-1020 nav-item">
                        <a href="/" class="nav-link">Inicio</a>
                     </li>
                     <li id="menu-item-1021" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-579 current_page_item menu-item-1021 nav-item active">
                        <a href="index.html" class="nav-link active">Servicios</a>
                     </li>
                     <li id="menu-item-1032" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1032 nav-item dropdown">
                        <a href="web/shop/index.html" class="nav-link dropdown-toggle" data-toggle="dropdown">Tiendas</a>
                        <ul class="dropdown-menu">
                           <li id="menu-item-1180" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1180 nav-item">
                              <a href="web/shop/index.html" class=" dropdown-item">Tienda 1</a>
                           </li>
                        </ul>
                     </li>
                     <li id="menu-item-1022" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1022 nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Nosotros</a>
                        <ul class="dropdown-menu">
                           <li id="menu-item-927" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-927 nav-item">
                              <a href="web/about-us/index.html" class=" dropdown-item">Nosotros</a>
                           </li>
                           <li id="menu-item-926" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-926 nav-item">
                              <a href="web/team/index.html" class=" dropdown-item">Nuestro Equipo</a>
                           </li>
                           <li id="menu-item-1026" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1026 nav-item">
                              <a href="web/blog/index.html" class=" dropdown-item">Blog</a>
                           </li>
                           <li id="menu-item-1024" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1024 nav-item">
                              <a href="web/testimonial/index.html" class=" dropdown-item">Comentarios</a>
                           </li>
                           <li id="menu-item-1023" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1023 nav-item">
                              <a href="web/gallery/index.html" class=" dropdown-item">Galeria</a>
                           </li>	
                           <li id="menu-item-925" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-925 nav-item">
                              <a href="web/faq/index.html" class=" dropdown-item">Preguntas</a>
                           </li>
                           <li id="menu-item-1513" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1513 nav-item">
                              <a href="web/trabaja-con-nostros/index.html" class=" dropdown-item">Trabaja con Nostros</a>
                           </li>
                        </ul>
                     </li>
                     <li id="menu-item-924" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-924 nav-item">
                        <a href="web/contact/index.html" class="nav-link">Contáctenos</a>
                     </li>
                     <li id="menu-item-924" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-924 nav-item">
                        <a href="{{ route('login') }}" class="nav-link">Ingresar</a>
                     </li>
                  </ul>
               </div>

                     <!-- collapse end -->
               <ul class="header-nav-right-info form-inline">
                  <li class="header-book-btn">
                     <a href="web/shop/index.html" class="btn btn-primary">PEDIDOS</a>
                  </li>
                  <li>
                     <div class="header-cart">
                        <div class="cart-link">
                           <a class="cart-contents" href="web/cart/index.html" title="View your shopping cart">
                              <span class="icon icon-tscart"></span>
                              <sup>0</sup>
                           </a>
                        </div>
                     </div>
                  </li>
                  <!-- off canvas -->
                  <li>
                     <a href="#" class="navSidebar-button">
                        <i class="icon icon-humburger"></i>
                     </a>
                  </li>
               </ul>
            </nav>
                  <!-- nav end -->
         </div>
               <!-- container end -->
      </div>
         <!-- navbar contianer end -->
   </header>

<!-- sidebar cart item -->
   <div class="xs-sidebar-group info-group">
      <div class="xs-overlay xs-bg-black"></div>
         <div class="xs-sidebar-widget">
         <div class="sidebar-widget-container">
               <div class="widget-heading">
                  <a href="#" class="close-side-widget">
                     <i class="icon icon-cross"></i>
                  </a>
               </div>
               <div class="sidebar-textwidget">
                  <div class="sidebar-logo-wraper">
                     <a class="navbar-brand" href="web/index.html">
                        <img src="web/wp-content/uploads/2021/03/Q%c2%b4Facil.png" alt="QFacil Perú">
                     </a>
                  </div>
                  <div class="off-canvas-desc">
                        Esto es una prueba de la misión de la empresa.                
                  </div>
                  <ul class="sideabr-list-widget">
                     <li>
                        <div class="media">
                           <div class="d-flex">
                              <i class="fa fa-envelope"></i>
                           </div>
                           <div class="media-body">
                              <span>ventas@qfacil.com</span>
                           </div>
                        </div><!-- address 1 -->
                     </li>
                        
                     <li>
                        <div class="media">
                           <div class="d-flex">
                              <i class="fa fa-phone"></i>
                           </div>
                           <div class="media-body">
                              <span>+51944646613</span>
                           </div>
                        </div><!-- address 1 -->
                     </li>
                  </ul>
                  <!-- .sideabr-list-widget -->

                  <ul class="social-list version-2">
                        <li class="ts-facebook">
                           <a title="facebook" href="#">
                              <i class="fa fa-facebook"></i>
                           </a>
                        </li>
                        <li class="ts-twitter">
                           <a title="Twitter" href="#">
                              <i class="fa fa-twitter"></i>
                           </a>
                        </li>
                        <li class="ts-linkedin">
                           <a title="Linkedin" href="#">
                              <i class="fa fa-linkedin"></i>
                           </a>
                        </li>
                        <li class="ts-youtube-play">
                           <a title="youtube" href="#">
                              <i class="fa fa-youtube-play"></i>
                           </a>
                        </li>
                  </ul>
               </div>
         </div>
      </div>
   </div>    <!-- END sidebar widget item -->    <!-- END offset cart strart -->

   <div id="post-579" class="home-full-width-content post-579 page type-page status-publish hentry" role="main">
	<div class="builder-content">
		<div data-elementor-type="wp-page" data-elementor-id="579" class="elementor elementor-579" data-elementor-settings="[]">
			<div class="elementor-inner">
				<div class="elementor-section-wrap">
               
               
               <section class="elementor-section elementor-top-section elementor-element elementor-element-ab401fe elementor-section-full_width elementor-section-height-min-height elementor-section-height-default elementor-section-items-middle" data-id="ab401fe" data-element_type="section" data-settings="{&quot;ekit_has_onepagescroll_dot&quot;:&quot;yes&quot;}">
                  <div class="elementor-container elementor-column-gap-no">
                     <div class="elementor-row">
               <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-059015f" data-id="059015f" data-element_type="column">
               <div class="elementor-column-wrap elementor-element-populated">
                     <div class="elementor-widget-wrap">
                  <div class="elementor-element elementor-element-44dd48a elementor-widget elementor-widget-slider_revolution" data-id="44dd48a" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="slider_revolution.default">
               <div class="elementor-widget-container">
               
               <div class="wp-block-themepunch-revslider">
               <!-- START Slider 1 REVOLUTION SLIDER 6.3.8 --><p class="rs-p-wp-fix"></p>
               <rs-module-wrap id="rev_slider_4_1_wrapper" data-source="gallery" style="background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
                  <rs-module id="rev_slider_4_1" style="" data-version="6.3.8">
                  <rs-slides>
                      @foreach ($banner as $b)
                      <rs-slide data-key="rs-9" data-title="Slide" data-thumb="{{ ('img/web/fndBanner.png') }}" data-anim="ei:d;eo:d;s:1630;r:default;sl:d;">
                        <img loading="lazy" src="{{ ('img/web/fndBanner.png') }}" title="slider_bg2" width="100%" height="100%" data-parallax="off" class="rev-slidebg" data-no-retina>
                        <rs-layer id="slider-4-slide-9-layer-2" data-type="text" data-color="#e7272d" data-rsp_ch="on" data-xy="xo:68px,56px,42px,30px;yo:276px,227px,172px,250px;" data-text="w:normal;s:22,18,13,18;l:22,18,13,18;fw:800;" data-frame_0="x:0,0,0,0px;y:0,0,0,0px;o:1;" data-frame_0_chars="d:5;x:0,0,0,0px;y:0,0,0,0px;o:0;rX:90deg;oZ:-50;" data-frame_1="x:0,0,0,0px;y:0,0,0,0px;st:750;sp:1240;sR:750;" data-frame_1_chars="e:power4.inOut;d:10;x:0,0,0,0px;y:0,0,0,0px;oZ:-50;" data-frame_999="o:0;st:w;sR:6110;" style="z-index:8;font-family:Barlow;text-transform:uppercase;">
                           <div class="subtitle">Lo que ofrecemos</div> 
                        </rs-layer>
                  
                     <rs-layer id="slider-4-slide-9-layer-3" data-type="text" data-rsp_ch="on" data-xy="xo:60px,49px,37px,29px;yo:317px,261px,198px,294px;" data-text="w:normal;s:70,90,68,41;l:100,82,62,38;fw:800;" data-dim="w:582,480,364,224;" data-ford="frame_0;frame_1;frame_2;frame_999;" data-frame_0="o:1;" data-frame_0_chars="d:5;x:-105%;o:0;rZ:-90deg;" data-frame_0_mask="u:t;" data-frame_1="st:580;sp:1200;" data-frame_1_chars="e:power4.inOut;dir:backward;d:10;rZ:0deg;" data-frame_1_mask="u:t;" data-frame_999="o:0;st:w;" data-frame_2="oX:50%;oY:50%;oZ:0;tp:600px;st:6810;sp:2190;sR:2160;" style="z-index:9;font-family:Barlow;text-transform:uppercase;">
                        {{ $b->titulo }}
                     </rs-layer>
               
                     <rs-layer id="slider-4-slide-9-layer-11" class="rs-pxl-3" data-type="image" data-rsp_ch="on" data-xy="xo:746px,616px,468px,291px;yo:275px,227px,172px,392px;" data-text="w:normal;s:20,16,12,7;l:0,20,15,9;" data-dim="w:505px,417px,316px,194px;h:222px,183px,139px,85px;" data-frame_0="x:100%;" data-frame_0_mask="u:t;" data-frame_1="st:2190;sp:820;sR:2190;" data-frame_1_mask="u:t;" data-frame_999="o:0;st:w;sR:5990;" style="z-index:11;">
                        <img loading="lazy" src="web/wp-content/uploads/2019/10/shape3-min.png" alt="shape3-min" width="574" height="252" data-no-retina> 
                     </rs-layer>
               
                     <rs-layer id="slider-4-slide-9-layer-12" class="rs-pxl-1" data-type="image" data-rsp_ch="on" data-xy="xo:751px,620px,471px,284px;yo:412px,340px,258px,341px;" data-text="w:normal;s:20,16,12,7;l:0,20,15,9;" data-dim="w:494px,407px,309px,190px;h:372px,307px,233px,143px;" data-frame_0="x:-100%;" data-frame_0_mask="u:t;" data-frame_1="st:1560;sp:1000;sR:1560;" data-frame_1_mask="u:t;" data-frame_999="o:0;st:w;sR:6440;" style="z-index:12;">
                        <img loading="lazy" src="web/wp-content/uploads/2019/10/shape1-min.png" alt="shape1-min" width="540" height="407" data-no-retina> 
                     </rs-layer>
                     
                     <rs-layer id="slider-4-slide-9-layer-14" data-type="text" data-rsp_ch="on" data-xy="xo:642px,530px,402px,357px;yo:674px,556px,422px,460px;" data-text="w:normal;s:20,16,12,17;l:25,20,15,22;" data-frame_0="x:0,0,0,0px;y:0,0,0,0px;" data-frame_1="x:0,0,0,0px;y:0,0,0,0px;st:1570;sR:1570;" data-frame_999="o:0;st:w;sR:7130;" style="z-index:18;font-family:Roboto;">
                        Costo 
                     </rs-layer>
               
                     <rs-layer id="slider-4-slide-9-layer-15" class="rs-pxl-2" data-type="image" data-rsp_ch="on" data-xy="xo:755px,623px,473px,293px;yo:185px,152px,115px,269px;" data-text="w:normal;s:20,16,12,7;l:0,20,15,9;" data-dim="w:491px,405px,307px,189px;h:712px,587px,445px,274px;" data-frame_0="y:-100%;" data-frame_0_mask="u:t;" data-frame_1="st:2140;sp:1200;sR:2140;" data-frame_1_mask="u:t;" data-frame_999="o:0;st:w;sR:5660;" style="z-index:13;">
                        <img loading="lazy" src="{{ $b->url }}" width="491" height="712" data-no-retina> 
                     </rs-layer>
                     
                     <rs-layer id="slider-4-slide-9-layer-16" class="rs-pxl-1" data-type="image" data-rsp_ch="on" data-xy="xo:744px,614px,466px,297px;yo:393px,324px,246px,329px;" data-text="w:normal;s:20,16,12,7;l:0,20,15,9;" data-dim="w:539px,445px,338px,208px;h:313px,258px,196px,120px;" data-frame_0="x:-100%;" data-frame_0_mask="u:t;" data-frame_1="st:2040;sp:1000;sR:2040;" data-frame_1_mask="u:t;" data-frame_999="o:0;st:w;sR:5960;" style="z-index:10;">
                        <img loading="lazy" src="web/wp-content/uploads/2019/10/shape2-min.png" alt="shape2-min" width="570" height="331" data-no-retina> 
                     </rs-layer>
                     
                     <rs-layer id="slider-4-slide-9-layer-17"  data-type="shape" data-rsp_ch="on" data-xy="xo:65px,53px,40px,16px;yo:653px,539px,409px,455px;" data-text="w:normal;s:20,16,12,7;l:0,20,15,9;" data-dim="w:803px,663px,503px,442px;h:120,99,75,64;" data-border="bor:5px,5px,5px,5px;" data-frame_1="st:1270;sp:800;sR:1270;" data-frame_999="o:0;st:w;sR:6930;" style="z-index:14;background-color:#e7272d;"> 
                     </rs-layer>
                     
                     <rs-layer id="slider-4-slide-9-layer-18" data-type="text" data-rsp_ch="on" data-xy="xo:638px,526px,399px,358px;yo:707px,583px,442px,488px;" data-text="w:normal;s:50,41,31,19;l:43,35,26,16;fw:800;" data-frame_1="st:1490;sR:1490;" data-frame_999="o:0;st:w;sR:7210;" style="z-index:19;font-family:Barlow;">
                        S/. {{ $b->costo }}
                     </rs-layer>
                     
                     <rs-layer id="slider-4-slide-9-layer-19" data-type="text" data-rsp_ch="on" data-xy="xo:392px,323px,245px,221px;yo:704px,581px,441px,480px;" data-text="w:normal;s:24,19,14,16;l:18,14,10,6;fw:700;" data-dim="w:119px,98px,74px,45px;" data-frame_1="st:1730;sR:1730;" data-frame_999="o:0;st:w;sR:6970;" style="z-index:16;font-family:Barlow;" >
                        <ul>
                           <li> {{ $b->caracteristica_1 }}</li>
                        </ul> 
                     </rs-layer>
                     
                     <rs-layer id="slider-4-slide-9-layer-20" class="item-first" data-type="text" data-rsp_ch="on" data-xy="xo:112px,92px,69px,35px;yo:705px,582px,442px,481px;" data-text="w:normal;s:24,19,14,15;l:18,14,10,6;fw:700;" data-dim="w:0px,1px,1px,49px;" data-frame_1="st:1900;sR:1900;" data-frame_999="o:0;st:w;sR:6800;" style="z-index:15;font-family:Barlow;">
                        <ul> 
                           <li> {{ $b->caracteristica_2 }} </li>
                        </ul> 
                     </rs-layer>
                     
                     <rs-layer id="slider-4-slide-9-layer-21"  data-type="object" data-rsp_ch="on" data-xy="xo:50px,41px,31px,19px;yo:311px,256px,194px,160px;" data-text="w:normal;s:20,16,12,7;l:0,20,15,9;" data-frame_1="st:1470;sR:1470;" data-frame_999="o:0;st:w;sR:7230;" style="z-index:20;font-family:Roboto;"> 
                     </rs-layer>
                     
                     <rs-layer id="slider-4-slide-9-layer-22" data-type="text" data-rsp_ch="on" data-xy="xo:248px,204px,154px,128px;yo:705px,582px,442px,480px;" data-text="w:normal;s:24,19,14,15;l:18,14,10,6;fw:700;" data-dim="w:119px,98px,74px,45px;" data-frame_1="st:1650;sR:1650;" data-frame_999="o:0;st:w;sR:7050;" style="z-index:17;font-family:Barlow;">
                        <ul> 
                           <li> {{ $b->caracteristica_3 }} </li>
                        </ul> 
                     </rs-layer>
                     
                     <rs-layer id="slider-4-slide-9-layer-23" class="rev-btn" data-type="button" data-rsp_ch="on" data-xy="xo:837px,691px,524px,435px;yo:683px,564px,428px,470px;" data-text="w:normal;s:25,20,15,14;l:45,37,28,27;fw:500;" data-dim="w:59,48,36,35px;h:59px,48px,36px,35px;minh:0,none,none,none;" data-padding="t:10,8,6,4;r:5,4,3,2;b:5,4,3,2;l:20,17,13,11;" data-border="bor:100px,100px,100px,100px;" data-frame_1="st:1400;sR:1400;" data-frame_999="o:0;st:w;sR:7300;" data-frame_hover="c:#fff;bgc:#000;bor:100px,100px,100px,100px;" style="z-index:21;background-color:rgba(255,255,255,1);font-family:Roboto;" >
                        <a href="#"> 
                           <i class="fa-plus"></i>
                        </a> 
                     </rs-layer>
                  </rs-slide>     
                      @endforeach
                     
               </rs-slides>
               </rs-module>
               
               <script type="text/javascript">
                  setREVStartSize({c: 'rev_slider_4_1',rl:[1240,1024,778,480],el:[1000,768,650,650],gw:[1240,1024,778,480],gh:[1000,768,650,650],type:'standard',justify:'',layout:'fullwidth',mh:"0"});
                  var	revapi4,
                     tpj;
                  function revinit_revslider41() {
                  jQuery(function() {
                     tpj = jQuery;
                     revapi4 = tpj("#rev_slider_4_1");
                     if(revapi4==undefined || revapi4.revolution == undefined){
                        revslider_showDoubleJqueryError("rev_slider_4_1");
                     }else{
                        revapi4.revolution({
                           sliderLayout:"fullwidth",
                           visibilityLevels:"1240,1024,778,480",
                           gridwidth:"1240,1024,778,480",
                           gridheight:"1000,768,650,650",
                           spinner:"spinner0",
                           perspectiveType:"local",
                           editorheight:"1000,768,650,650",
                           responsiveLevels:"1240,1024,778,480",
                           progressBar:{disableProgressBar:true},
                           navigation: {
                              onHoverStop:false,
                              arrows: {
                                 enable:true,
                                 style:"gloreya",
                                 hide_onmobile:true,
                                 hide_under:"1200px",
                                 left: {
                                    container:"layergrid",
                                    v_align:"bottom",
                                    h_offset:50,
                                    v_offset:115
                                 },
                                 right: {
                                    container:"layergrid",
                                    h_align:"left",
                                    v_align:"bottom",
                                    h_offset:165,
                                    v_offset:115
                                 }
                              },
                              bullets: {
                                 enable:true,
                                 tmp:"",
                                 style:"gloreya",
                                 anim:"zoomout",
                                 h_align:"left",
                                 h_offset:100,
                                 v_offset:120,
                                 container:"layergrid"
                              }
                           },
                           parallax: {
                              levels:[5,10,15,20,25,30,35,40,45,46,47,48,49,50,51,30],
                              type:"mouse",
                              origo:"slideCenter",
                              speed:0
                           },
                           fallbacks: {
                              allowHTML5AutoPlayOnAndroid:true
                           },
                        });
                     }
                     
                  });
                  } // End of RevInitScript
                  var once_revslider41 = false;
                  if (document.readyState === "loading") {document.addEventListener('readystatechange',function() { if((document.readyState === "interactive" || document.readyState === "complete") && !once_revslider41 ) { once_revslider41 = true; revinit_revslider41();}});} else {once_revslider41 = true; revinit_revslider41();}
               </script>
               <script>
                  var htmlDivCss = unescape("%5Ce825%23rev_slider_4_1_wrapper%20.gloreya.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3A%23000%3B%0A%09background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%09width%3A40px%3B%0A%09height%3A40px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A1000%3B%0A%7D%0A%23rev_slider_4_1_wrapper%20.gloreya.tparrows%3Ahover%20%7B%0A%09background%3A%23000%3B%0A%7D%0A%23rev_slider_4_1_wrapper%20.gloreya.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%27revicons%27%3B%0A%09font-size%3A15px%3B%0A%09color%3A%23fff%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%2040px%3B%0A%09text-align%3A%20center%3B%0A%7D%0A%23rev_slider_4_1_wrapper%20.gloreya.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%27%5Ce824%27%3B%0A%7D%0A%23rev_slider_4_1_wrapper%20.gloreya.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%27%5Ce825%27%3B%0A%7D%0A%0A%0A%23rev_slider_4_1_wrapper%20.gloreya.tp-bullets%20%7B%0A%7D%0A%0A%23rev_slider_4_1_wrapper%20.gloreya%20.tp-bullet%20%7B%0A%20%20%20%20overflow%3Ahidden%3B%0A%20%20%20%20border-radius%3A50%25%3B%0A%20%20%20%20width%3A16px%3B%0A%20%20%20%20height%3A16px%3B%0A%20%20%20%20background-color%3A%20rgba%280%2C%200%2C%200%2C%200%29%3B%0A%20%20%20%20box-shadow%3A%20inset%200%200%200%202px%20%23ffffff%3B%0A%20%20%20%20-webkit-transition%3A%20background%200.3s%20ease%3B%0A%20%20%20%20transition%3A%20background%200.3s%20ease%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%7D%0A%0A%23rev_slider_4_1_wrapper%20.gloreya%20.tp-bullet%3Ahover%20%7B%0A%09%20%20background-color%3A%20rgba%280%2C0%2C0%2C0.21%29%3B%0A%7D%0A%23rev_slider_4_1_wrapper%20.gloreya%20.tp-bullet%3Aafter%20%7B%0A%20%20content%3A%20%27%20%27%3B%0A%20%20position%3A%20absolute%3B%0A%20%20bottom%3A%200%3B%0A%20%20height%3A%200%3B%0A%20%20left%3A%200%3B%0A%20%20width%3A%20100%25%3B%0A%20%20background-color%3A%20%23ffffff%3B%0A%20%20box-shadow%3A%200%200%201px%20%23ffffff%3B%0A%20%20-webkit-transition%3A%20height%200.3s%20ease%3B%0A%20%20transition%3A%20height%200.3s%20ease%3B%0A%7D%0A%23rev_slider_4_1_wrapper%20.gloreya%20.tp-bullet.selected%3Aafter%20%7B%0A%20%20height%3A100%25%3B%0A%7D%0A%0A");
                  var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                  if(htmlDiv) {
                     htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                  }else{
                     var htmlDiv = document.createElement('div');
                     htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                     document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                  }
               </script>
               <script>
                  var htmlDivCss = unescape("%0A%0A%0A%0A%0A%0A");
                  var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                  if(htmlDiv) {
                     htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                  }else{
                     var htmlDiv = document.createElement('div');
                     htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                     document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                  }
               </script>
               </rs-module-wrap>
               <!-- END REVOLUTION SLIDER -->
               </div>
               
               </div>
               </div>
                  </div>
               </div>
               </div>
                        </div>
               </div>
               </section>
               
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-f6b5ea0 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="f6b5ea0" data-element_type="section" data-settings="{&quot;ekit_has_onepagescroll_dot&quot;:&quot;yes&quot;}">
                     <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-row">
                           <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-63b9ef4" data-id="63b9ef4" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-widget-wrap">
                                 <div class="elementor-element elementor-element-edc406f elementor-widget elementor-widget-image" data-id="edc406f" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                    <div class="elementor-widget-container">
                                       <div class="elementor-image">
                                          <img width="800" height="12" src="{{ ('img/web/separador.png') }}" class="attachment-large size-large" alt="" loading="lazy" srcset="{{ ('img/web/separador.png') }} 1024w, {{ ('img/web/separador.png') }} 600w, {{ ('img/web/separador.png') }} 300w, {{ ('img/web/separador.png') }} 768w, {{ ('img/web/separador.png') }} 1536w, {{ ('img/web/separador.png') }} 1920w" sizes="(max-width: 800px) 100vw, 800px" />											
                                       </div>
                                    </div>
                                 </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
               
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-5b862cc elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="5b862cc" data-element_type="section" data-settings="{&quot;ekit_has_onepagescroll_dot&quot;:&quot;yes&quot;}">
                     <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                           <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f6980b1" data-id="f6980b1" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-c45ebcc elementor-widget elementor-widget-gloreya-product-slider" data-id="c45ebcc" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="gloreya-product-slider.default">
                                       <div class="elementor-widget-container">
                                          <div class="ts-product-item ts-product-slider owl-carousel" data-controls="{&quot;nav&quot;:&quot;yes&quot;,&quot;auto_play&quot;:&quot;&quot;,&quot;post_count&quot;:4}">
                                            @foreach ($servicios as $s)
                                            
                                            <div class="ts-product-single-item">
                                                <img class="border-shap" src="web/wp-content/themes/gloreya/assets/images/menu_border_shape.png" alt="Smoked Brisket">
                                                <div class="shap-img">
                                                   <img src="{{ $s->fondo }}" alt="Smoked Brisket">
                                                </div>
                                                <h3 class="ts-title"> 
                                                   <a href="product/smoked-brisket/index.html"> {{ $s->servicio }}
                                                </h3>
                                                <p>{{ $s->descripcion }}
                                                <h4 class="product-price">S/. {{ $s->costo }}</h4>
                                                <div class="product-btn">
                                                   <a href="{{ $s->botonslug }}" class="btn btn-primary"><i class="icon icon-tscart"></i>ORDENAR</a>
                                                </div>
                                                <div class="product-img">
                                                   <img class="img-fluid" src="{{ $s->url }}" alt="Smoked Brisket">
                                                </div>
                                             </div>

                                            @endforeach 
                                                
                                          </div><!-- block-item6 -->
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
               
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-c8bb82e elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="c8bb82e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;ekit_has_onepagescroll_dot&quot;:&quot;yes&quot;}">
                     <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                           <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c00fc2c" data-id="c00fc2c" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-f09f99a elementor-widget elementor-widget-gloreya-title" data-id="f09f99a" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="gloreya-title.default">
                                       <div class="elementor-widget-container">
                                          <div class="ts-section-title title-center">
                                             <h2 class="section-title ">Nuestras Categorías     </h2>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
               
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-cb9d626 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="cb9d626" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;ekit_has_onepagescroll_dot&quot;:&quot;yes&quot;}">
                     <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                           <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-47fc934" data-id="47fc934" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-fdc054e elementor-widget elementor-widget-gloreya-food-menu" data-id="fdc054e" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="gloreya-food-menu.default">
                                       <div class="elementor-widget-container">
                                          <div class="row ts-tab-menu">
                                             <div class="col d-flex justify-content-center">
                                                <ul class="nav nav-menu-tabs" role="tablist">
                                                    @foreach ($categoria as $c)
                                                    <li class="col" onclick="verProductos({{ $c->id }})">
                                                        <a class=" show " data-toggle="tab" href="#{{ $c->categoria }}" role="tab" aria-controls="{{ $c->categoria }}" aria-selected="true">
                                                           <img src="{{ $c->url }}" class="img-fluid" alt='{{ $c->categoria }}'/>
                                                           {{ $c->categoria }}                               
                                                        </a>
                                                     </li>     
                                                    @endforeach
                                                </ul>
                                             </div>
                                          </div>
               
                                          <div class="tab-content">
                                             <div id="burgers" class="container tab-pane active">
                                                <div class="row menu-section" id="tbProductos"> 
                                                    
                                                    @foreach ($producto as $p)
                                                    <div class="menu-block col-xs-12 col-md-6 col-sm-6"> 
                                                        <div class="inner-box"> 
                                                           <span class="menu-tag">Calificación: {{ $p->calificacion }}</span>
                                                           <div class="info clearfix">
                                                              <h3 class="post-title pull-left">
                                                                 <a href="#">{{ $p->nombre }}</a>
                                                              </h3> 
                                                              <h3 class="price pull-right"><i>S/. </i> {{ $p->precio }} </h3>
                 
                                                           </div>
                                                           <p class="ingradien-text"> {{ $p->descripcion }}</p>
                                                        </div>
                                                     </div>     
                                                    @endforeach

                                             </div>
                                          </div>
                                          <div id="pizzas" class="container tab-pane ">
                                             <div class="row menu-section"> 
                                                
                                             </div>
                                          </div>
               
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  </section>
               
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-3929ac5 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="3929ac5" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;ekit_section_parallax_multi_items&quot;:[{&quot;parallax_style&quot;:&quot;animation&quot;,&quot;item_source&quot;:&quot;image&quot;,&quot;image&quot;:{&quot;url&quot;:&quot;https:\/\/www.qfacilperu.com\/wp-content\/uploads\/2019\/10\/testimonial_pattern-min.png&quot;,&quot;id&quot;:284},&quot;shape&quot;:null,&quot;shape_color&quot;:null,&quot;width_type&quot;:&quot;&quot;,&quot;width_type_tablet&quot;:&quot;&quot;,&quot;width_type_mobile&quot;:&quot;&quot;,&quot;custom_width&quot;:null,&quot;custom_width_tablet&quot;:null,&quot;custom_width_mobile&quot;:null,&quot;source_rotate&quot;:{&quot;unit&quot;:&quot;deg&quot;,&quot;size&quot;:0,&quot;sizes&quot;:[]},&quot;source_rotate_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;source_rotate_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_x&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:14,&quot;sizes&quot;:[]},&quot;pos_x_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_x_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_y&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:25,&quot;sizes&quot;:[]},&quot;pos_y_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_y_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;animation&quot;:&quot;tada&quot;,&quot;animation_tablet&quot;:&quot;&quot;,&quot;animation_mobile&quot;:&quot;&quot;,&quot;item_opacity&quot;:&quot;1&quot;,&quot;animation_speed&quot;:&quot;5&quot;,&quot;animation_iteration_count&quot;:&quot;infinite&quot;,&quot;animation_direction&quot;:&quot;alternate&quot;,&quot;parallax_speed&quot;:null,&quot;parallax_transform&quot;:null,&quot;parallax_transform_value&quot;:null,&quot;smoothness&quot;:null,&quot;offsettop&quot;:null,&quot;offsetbottom&quot;:null,&quot;maxtilt&quot;:null,&quot;scale&quot;:null,&quot;disableaxis&quot;:null,&quot;zindex&quot;:&quot;2&quot;,&quot;_id&quot;:&quot;ff186cb&quot;,&quot;item_opacity_tablet&quot;:&quot;&quot;,&quot;item_opacity_mobile&quot;:&quot;&quot;,&quot;left_to_right&quot;:&quot;left&quot;,&quot;left&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:17,&quot;sizes&quot;:[]},&quot;left_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;left_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;right&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;right_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;right_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;top_to_bottom&quot;:&quot;top&quot;,&quot;pos_y_bottom&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;pos_y_bottom_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_y_bottom_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;parallax_blur_effect&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:0,&quot;sizes&quot;:[]},&quot;parallax_blur_effect_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;parallax_blur_effect_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}}],&quot;ekit_section_parallax_multi&quot;:&quot;yes&quot;,&quot;ekit_has_onepagescroll_dot&quot;:&quot;yes&quot;}">
                     <div class="elementor-background-overlay"></div>
                     <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                           <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-900ad31" data-id="900ad31" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-d88e5ae elementor-widget elementor-widget-heading" data-id="d88e5ae" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                                       <div class="elementor-widget-container">
                                          <h2 class="elementor-heading-title elementor-size-default">Comentarios</h2>		
                                       </div>
                                    </div>
                                    <div class="elementor-element elementor-element-e5a8495 elementor-widget elementor-widget-gloreya-title" data-id="e5a8495" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="gloreya-title.default">
                                       <div class="elementor-widget-container">
                                          <div class="ts-section-title title-center">
                                             <h2 class="section-title ">Nuestros Clientes     </h2>
                                          </div>
                                       </div>
                                    </div>
               
                                    <div class="elementor-element elementor-element-2b8b41b elementor-widget elementor-widget-gloreya-testimonial" data-id="2b8b41b" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="gloreya-testimonial.default">
                                       <div class="elementor-widget-container">
                                          <div  class="ts-testimonial ts-testimonial-classic testimonial-carousel owl-carousel style4" data-controls="{&quot;nav&quot;:&quot;&quot;,&quot;dot&quot;:&quot;yes&quot;,&quot;auto_play&quot;:&quot;&quot;,&quot;auto_loop&quot;:&quot;yes&quot;}">
                                              @foreach ($comentario as $c)
                                              <div class="row">
                                                <div class="col-lg-6">
                                                   <div class="testimonial-thumb">
                                                      <img src="{{ $c->foto }}" alt="Nina Margaret" class="img-fluid testimonial-img">
                                                   </div>
                                                </div>
                                                <div class="col-lg-6 ">
                                                   <div class="testimonial-author-content two">
                                                      <p class="testimonial-text">
                                                         <i class="fa fa-quote-left" aria-hidden="true"> </i> 
                                                         {{$c->comentario}}
                                                      </p>
                                                      <div class="testimonial-footer media">
                                                         <div class="testimonial-info align-self-center">
                                                            <h4 class="author-name">{{ $c->titulo }}</h4>
                                                            <span class="author-designation">{{ $c->usuario }}</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>     
                                              @endforeach
                                             
                                          </div>
                                       </div>      
                                    </div>
               
                                    <div class="elementor-element elementor-element-9ac6546 elementor-widget__width-initial elementor-absolute elementor-hidden-tablet elementor-hidden-phone elementor-widget-divider--view-line elementor-widget elementor-widget-divider" data-id="9ac6546" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="divider.default">
                                       <div class="elementor-widget-container">
                                          <div class="elementor-divider">
                                             <span class="elementor-divider-separator"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
               
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-94893c3 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle" data-id="94893c3" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;ekit_has_onepagescroll_dot&quot;:&quot;yes&quot;}">
                     <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                           <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-9421064" data-id="9421064" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-83e0074 elementor-widget elementor-widget-gloreya-video-popup" data-id="83e0074" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="gloreya-video-popup.default">
                                       <div class="elementor-widget-container">
                                          <div class="video-icon">
                                             <a href="www.youtube.com/watch83e4.html?v=7LjC-nUPrNg" class="ts-play-btn video-btn">
                                                <i class="fa fa-play-circle"></i> 
                                                <span class="btn-hover-anim"></span>
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
               
               
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-14ae750 ts-custom-gallery  gallery-img-animation elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="14ae750" data-element_type="section" data-settings="{&quot;ekit_section_parallax_multi&quot;:&quot;yes&quot;,&quot;ekit_section_parallax_multi_items&quot;:[{&quot;parallax_style&quot;:&quot;animation&quot;,&quot;item_source&quot;:&quot;image&quot;,&quot;image&quot;:{&quot;url&quot;:&quot;https:\/\/www.qfacilperu.com\/wp-content\/uploads\/2019\/10\/testimonial_pattern-min.png&quot;,&quot;id&quot;:284},&quot;shape&quot;:null,&quot;shape_color&quot;:null,&quot;width_type&quot;:&quot;&quot;,&quot;width_type_tablet&quot;:&quot;&quot;,&quot;width_type_mobile&quot;:&quot;custom&quot;,&quot;custom_width&quot;:null,&quot;custom_width_tablet&quot;:null,&quot;custom_width_mobile&quot;:null,&quot;source_rotate&quot;:{&quot;unit&quot;:&quot;deg&quot;,&quot;size&quot;:0,&quot;sizes&quot;:[]},&quot;source_rotate_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;source_rotate_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_x&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:59,&quot;sizes&quot;:[]},&quot;pos_x_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_x_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_y&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:2,&quot;sizes&quot;:[]},&quot;pos_y_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_y_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;animation&quot;:&quot;swing&quot;,&quot;animation_tablet&quot;:&quot;&quot;,&quot;animation_mobile&quot;:&quot;&quot;,&quot;item_opacity&quot;:1,&quot;animation_speed&quot;:7,&quot;animation_iteration_count&quot;:&quot;infinite&quot;,&quot;animation_direction&quot;:&quot;normal&quot;,&quot;parallax_speed&quot;:null,&quot;parallax_transform&quot;:null,&quot;parallax_transform_value&quot;:null,&quot;smoothness&quot;:null,&quot;offsettop&quot;:null,&quot;offsetbottom&quot;:null,&quot;maxtilt&quot;:null,&quot;scale&quot;:null,&quot;disableaxis&quot;:null,&quot;zindex&quot;:0,&quot;_id&quot;:&quot;ffc71ff&quot;,&quot;item_opacity_tablet&quot;:&quot;&quot;,&quot;item_opacity_mobile&quot;:0,&quot;left_to_right&quot;:&quot;right&quot;,&quot;left&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;left_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;left_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;right&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:30,&quot;sizes&quot;:[]},&quot;right_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;right_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;top_to_bottom&quot;:&quot;top&quot;,&quot;pos_y_bottom&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;pos_y_bottom_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_y_bottom_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;parallax_blur_effect&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:0,&quot;sizes&quot;:[]},&quot;parallax_blur_effect_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;parallax_blur_effect_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}},{&quot;parallax_style&quot;:&quot;animation&quot;,&quot;item_source&quot;:&quot;image&quot;,&quot;image&quot;:{&quot;url&quot;:&quot;https:\/\/www.qfacilperu.com\/wp-content\/uploads\/2019\/10\/testimonial_pattern-min.png&quot;,&quot;id&quot;:284},&quot;shape&quot;:null,&quot;shape_color&quot;:null,&quot;width_type&quot;:&quot;custom&quot;,&quot;width_type_tablet&quot;:&quot;&quot;,&quot;width_type_mobile&quot;:&quot;&quot;,&quot;custom_width&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;custom_width_tablet&quot;:null,&quot;custom_width_mobile&quot;:null,&quot;source_rotate&quot;:{&quot;unit&quot;:&quot;deg&quot;,&quot;size&quot;:0,&quot;sizes&quot;:[]},&quot;source_rotate_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;source_rotate_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_x&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:29,&quot;sizes&quot;:[]},&quot;pos_x_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_x_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_y&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:42,&quot;sizes&quot;:[]},&quot;pos_y_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_y_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;animation&quot;:&quot;tada&quot;,&quot;animation_tablet&quot;:&quot;&quot;,&quot;animation_mobile&quot;:&quot;&quot;,&quot;item_opacity&quot;:1,&quot;animation_speed&quot;:10,&quot;animation_iteration_count&quot;:&quot;infinite&quot;,&quot;animation_direction&quot;:&quot;normal&quot;,&quot;parallax_speed&quot;:null,&quot;parallax_transform&quot;:null,&quot;parallax_transform_value&quot;:null,&quot;smoothness&quot;:null,&quot;offsettop&quot;:null,&quot;offsetbottom&quot;:null,&quot;maxtilt&quot;:null,&quot;scale&quot;:null,&quot;disableaxis&quot;:null,&quot;zindex&quot;:0,&quot;_id&quot;:&quot;8c42e64&quot;,&quot;item_opacity_tablet&quot;:&quot;&quot;,&quot;item_opacity_mobile&quot;:0,&quot;left_to_right&quot;:&quot;left&quot;,&quot;left&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:30,&quot;sizes&quot;:[]},&quot;left_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;left_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;right&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;right_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;right_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;top_to_bottom&quot;:&quot;bottom&quot;,&quot;pos_y_bottom&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:20,&quot;sizes&quot;:[]},&quot;pos_y_bottom_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;pos_y_bottom_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;parallax_blur_effect&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:0,&quot;sizes&quot;:[]},&quot;parallax_blur_effect_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;parallax_blur_effect_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}}],&quot;ekit_has_onepagescroll_dot&quot;:&quot;yes&quot;}">
                     <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                           <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-757c371" data-id="757c371" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-5e5dff0 gallery-spacing-custom elementor-widget elementor-widget-image-gallery" data-id="5e5dff0" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="image-gallery.default">
                                       <div class="elementor-widget-container">
                                          <div class="elementor-image-gallery">
                                             <div id='gallery-1' class='gallery galleryid-33 gallery-columns-4 gallery-size-full'>
                                                <figure class='gallery-item'>
                                                   <div class='gallery-icon landscape'>
                                                      <a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="5e5dff0" data-elementor-lightbox-title="news_image3-min" href='img/web/news_image3-min.jpg'>
                                                         <img width="825" height="559" src="img/web/news_image3-min.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image3-min.jpg 825w, https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image3-min-600x407.jpg 600w, https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image3-min-300x203.jpg 300w, https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image3-min-768x520.jpg 768w" sizes="(max-width: 825px) 100vw, 825px" />
                                                      </a>
                                                   </div>
                                                </figure>
                                                <figure class='gallery-item'>
                                                   <div class='gallery-icon landscape'>
                                                      <a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="5e5dff0" data-elementor-lightbox-title="gallery_image1-min" href='wp-content/uploads/2019/10/gallery_image1-min-1.jpg'>
                                                         <img width="800" height="800" src="wp-content/uploads/2019/10/gallery_image1-min-1.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image1-min-1.jpg 800w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image1-min-1-600x600.jpg 600w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image1-min-1-300x300.jpg 300w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image1-min-1-150x150.jpg 150w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image1-min-1-768x768.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" />
                                                      </a>
                                                   </div>
                                                </figure>
                                                <figure class='gallery-item'>
                                                   <div class='gallery-icon landscape'>
                                                      <a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="5e5dff0" data-elementor-lightbox-title="news_image2-min" href='wp-content/uploads/2019/10/news_image2-min.jpg'>
                                                         <img width="825" height="559" src="wp-content/uploads/2019/10/news_image2-min.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image2-min.jpg 825w, https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image2-min-600x407.jpg 600w, https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image2-min-300x203.jpg 300w, https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image2-min-768x520.jpg 768w" sizes="(max-width: 825px) 100vw, 825px" />
                                                      </a>
                                                   </div>
                                                </figure>
                                                <figure class='gallery-item'>
                                                   <div class='gallery-icon landscape'>
                                                      <a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="5e5dff0" data-elementor-lightbox-title="gallery_image5-min" href='wp-content/uploads/2019/10/gallery_image5-min.jpg'>
                                                         <img width="800" height="800" src="wp-content/uploads/2019/10/gallery_image5-min.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image5-min.jpg 800w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image5-min-600x600.jpg 600w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image5-min-300x300.jpg 300w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image5-min-150x150.jpg 150w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image5-min-768x768.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" />
                                                      </a>
                                                   </div>
                                                </figure>
                                                <figure class='gallery-item'>
                                                   <div class='gallery-icon landscape'>
                                                      <a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="5e5dff0" data-elementor-lightbox-title="news_image1-min" href='wp-content/uploads/2019/10/news_image1-min.jpg'>
                                                         <img width="825" height="559" src="wp-content/uploads/2019/10/news_image1-min.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image1-min.jpg 825w, https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image1-min-600x407.jpg 600w, https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image1-min-300x203.jpg 300w, https://www.qfacilperu.com/wp-content/uploads/2019/10/news_image1-min-768x520.jpg 768w" sizes="(max-width: 825px) 100vw, 825px" />
                                                      </a>
                                                   </div>
                                                </figure>
                                                <figure class='gallery-item'>
                                                   <div class='gallery-icon landscape'>
                                                      <a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="5e5dff0" data-elementor-lightbox-title="gallery_image6-min" href='wp-content/uploads/2019/10/gallery_image6-min.jpg'>
                                                         <img width="800" height="800" src="wp-content/uploads/2019/10/gallery_image6-min.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image6-min.jpg 800w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image6-min-600x600.jpg 600w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image6-min-300x300.jpg 300w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image6-min-150x150.jpg 150w, https://www.qfacilperu.com/wp-content/uploads/2019/10/gallery_image6-min-768x768.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" />
                                                      </a>
                                                   </div>
                                                </figure>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
               
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-3f1f4c0 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="3f1f4c0" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;ekit_has_onepagescroll_dot&quot;:&quot;yes&quot;}">
                     <div class="elementor-background-overlay"></div>
                     <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                           <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e733c60" data-id="e733c60" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-23dedd2 elementor-widget elementor-widget-gloreya-title" data-id="23dedd2" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="gloreya-title.default">
                                       <div class="elementor-widget-container">
                                          <div class="ts-section-title title-center">
                                             <h2 class="section-title ">OFERTAS     </h2>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
               
                  <section class="elementor-section elementor-top-section elementor-element elementor-element-1e7da17 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1e7da17" data-element_type="section" data-settings="{&quot;ekit_has_onepagescroll_dot&quot;:&quot;yes&quot;}">
                     <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                           <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3b09f44" data-id="3b09f44" data-element_type="column">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-widget-wrap">
                                    <div class="elementor-element elementor-element-485878d elementor-widget elementor-widget-gloreya-latestnews" data-id="485878d" data-element_type="widget" data-settings="{&quot;ekit_we_effect_on&quot;:&quot;none&quot;}" data-widget_type="gloreya-latestnews.default">
                                       <div class="elementor-widget-container">
                                          <div class="row latest-blog">
                                             <div class="col-lg-4 fadeInUp">
                                                <div class="post">
                                                   <div class="post-media post-image">
                                                      <a href="2019/10/10/labor-depar-rules-pro-as-tweaks-overtime/index.html">
                                                         <img src="wp-content/uploads/2019/10/news_image3-min.jpg" class="img-fluid" alt="https://www.qfacilperu.com/author/admin/">
                                                      </a>
                                                   </div>
                                                   <div class="post-body">
                                                      <div class="post-meta">
                                                         <span class="post-author">
                                                            <i class="icon icon-user"></i> 
                                                            <a href="author/admin/index.html"> admin</a>
                                                         </span>
                                                         <span class="post-meta-date">
                                                            <i class="icon icon-clock"></i>
                                                            10 , October , 2019 
                                                         </span>
                                                      </div>
                                                      <div class="entry-header">
                                                         <h2 class="entry-title">
                                                            <a href="index1d2c.html?post_type=post&amp;p=354">Labor Depar rules </a>
                                                         </h2>
                                                      </div>
                  <!-- header end -->
                                                      <div class="entry-content">
                                                         <p> You’re cooking a meal, especially a holiday meal, to be   </p>
                                                      </div>
               
                                                      <div class="post-footer">
                                                         <a href="index1d2c.html?post_type=post&amp;p=354" class="btn-link"> Read More <i class="icon icon-arrow-right"></i>
                                                         </a>
                                                      </div>
                                                   </div>
                  <!-- post-body end -->
                                                </div>
                  <!-- post end-->
                                             </div>
                                             <div class="col-lg-4 fadeInUp">
                                                <div class="post">
                                                   <div class="post-media post-image">
                                                      <a href="2019/10/10/new-restaurant-in-town-that-looking-think-that/index.html">
                                                         <img src="wp-content/uploads/2019/10/news_image2-min.jpg" class="img-fluid" alt="https://www.qfacilperu.com/author/admin/">
                                                      </a>
                                                   </div>
                                                   <div class="post-body">
                                                      <div class="post-meta">
                                                         <span class="post-author">
                                                            <i class="icon icon-user"></i> 
                                                            <a href="author/admin/index.html"> admin</a>
                                                         </span>
               
                                                         <span class="post-meta-date">
                                                            <i class="icon icon-clock"></i>
                                                            10 , October , 2019 
                                                         </span>
                                                      </div>
                                                      <div class="entry-header">
                                                         <h2 class="entry-title">
                                                            <a href="indexa8ff.html?post_type=post&amp;p=351">New restaurant town </a>
                                                         </h2>
                                                      </div>
                  <!-- header end -->
                                                      <div class="entry-content">
                                                         <p> You’re cooking a meal, especially a holiday meal, to be   </p>
                                                      </div>
               
                                                      <div class="post-footer">
                                                         <a href="indexa8ff.html?post_type=post&amp;p=351" class="btn-link"> Read More <i class="icon icon-arrow-right"></i></a>
                                                      </div>
                                                   </div>
                  <!-- post-body end -->
                                                </div>
                  <!-- post end-->
                                             </div>
                                             <div class="col-lg-4 fadeInUp">
                                                <div class="post">
                                                   <div class="post-media post-image">
                                                      <a href="2019/10/10/check-upcoming-events/index.html"><img src="wp-content/uploads/2019/10/news_image1-min.jpg" class="img-fluid" alt="https://www.qfacilperu.com/author/admin/"></a>
                                                   </div>
                                                   <div class="post-body">
                                                      <div class="post-meta">
                                                         <span class="post-author">
                                                            <i class="icon icon-user"></i> 
                                                            <a href="author/admin/index.html"> admin</a>
                                                         </span>
               
                                                         <span class="post-meta-date">
                                                            <i class="icon icon-clock"></i>
                                                            10 , October , 2019 
                                                         </span>
                                                      </div>
                                                      <div class="entry-header">
                                                         <h2 class="entry-title">
                                                            <a href="index75a3.html?post_type=post&amp;p=346">Starbucks invests $100M </a>
                                                         </h2>
                                                      </div>
                  <!-- header end -->
                                                      <div class="entry-content">
                                                         <p> You’re cooking a meal, especially a holiday meal, to be   </p>
                                                      </div>
               
                                                      <div class="post-footer">
                                                         <a href="index75a3.html?post_type=post&amp;p=346" class="btn-link"> Read More <i class="icon icon-arrow-right"></i></a>
                                                      </div>
                                                   </div>
                  <!-- post-body end -->
                                                </div>
                  <!-- post end-->
                                             </div>
                                          </div> <!-- row end -->
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
				
				</div>
			</div>
		</div>
	</div> <!-- end main-content -->
</div> <!-- end main-content -->


  

 
   
<footer class="ts-footer solid-bg-two" >
   <div class="container">
      <div class="footer-logo-area text-center">
         <a class="footer-logo" href="web/index.html">
            <img src="web/wp-content/uploads/2021/03/Q%c2%b4Facil-1.png" alt="QFacil Perú">
         </a>
      </div>

      <div class="row">
         <div class="col-lg-3 col-md-6">
            <div class="footer-widget footer-left-widget">
               <h3 class="widget-title">Address</h3>			
               <div class="textwidget">
                  <p>570 8th Ave, <br /> New York, NY 10018<br /> United States</p>
               </div>
            </div>                    
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="footer-widget footer-two-widget">
               <h3 class="widget-title">Book a table</h3>			
               <div class="textwidget">
                  <p>Dogfood och Sliders foodtruck. <br /> Under Om oss kan ni läsa<br />
                     <strong>
                        <a href="tel:123-456-7890">(850) 435-4155</a>
                     </strong>
                  </p>
               </div>
            </div>                     
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="footer-widget footer-three-widget">
               <h3 class="widget-title">Opening hours</h3>			
               <div class="textwidget">
                  <p>Monday &#8211; Friday <br /> 10.00 AM &#8211; 11.00 PM</p>
               </div>
            </div>                     
         </div>
         <div class="col-lg-3 col-md-6">
            <h3 class="widget-title">Instagram feed</h3>        
            <div class="instagram_photo">
               <div id="instafeed" class="feed-content" data-token="2367672995.1677ed0.dea7a14501d04cd9982c7a0d23c716dd" data-media-count="3"></div>  
            </div>
         </div>                     
      </div>
<!-- end col -->
   </div>
   <div class='footer-bar'> </div>

   <div class="row copyright">
      <div class="col-lg-6 col-md-7 align-self-center">
         <div class="copyright-text">
            <div id="footer-nav" class="menu-footer-menu-container">
               <ul id="footer-menu" class="footer-menu">
                  <li id="menu-item-1309" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1309 nav-item">
                     <a href="web/about-us/index.html" class="nav-link">Nosotros</a>
                  </li>
                  <li id="menu-item-1308" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-579 current_page_item menu-item-1308 nav-item active">
                     <a href="index.html" class="nav-link active">Menu</a>
                  </li>
                  <li id="menu-item-1305" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1305 nav-item">
                     <a href="web/blog/index.html" class="nav-link">Blog</a>
                  </li>
                  <li id="menu-item-1304" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1304 nav-item">
                     <a href="web/gallery/index.html" class="nav-link">Galeria</a>
                  </li>
                  <li id="menu-item-1310" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1310 nav-item">
                     <a href="web/faq/index.html" class="nav-link">Preguntas Frecuentes</a>
                  </li>
                  <li id="menu-item-1306" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1306 nav-item">
                     <a href="web/contact/index.html" class="nav-link">Contactenos</a>
                  </li>
               </ul>
            </div>    
            &copy; 2019, Gloreya. All rights reserved                        </div>
         </div>
         <div class="col-lg-6 col-md-5 align-self-center">
            <div class="footer-social">
               <ul>
                  <li class="ts-facebook">
                     <a href="#" target="_blank">
                        <i class="fa fa-facebook"></i>
                     </a>
                  </li>
                  <li class="ts-twitter">
                     <a href="#" target="_blank">
                        <i class="fa fa-twitter"></i>
                     </a>
                  </li>
                  <li class="ts-instagram">
                     <a href="#" target="_blank">
                        <i class="fa fa-instagram"></i>
                     </a>
                  </li>
                  <li class="ts-youtube-play">
                     <a href="#" target="_blank">
                        <i class="fa fa-youtube-play"></i>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</footer>
        <!-- end footer -->
<div class="BackTo">
   <a href="#" class="fa fa-angle-up" aria-hidden="true"></a>
</div>
   
<script type="text/javascript">
   (function () {
   var c = document.body.className;
   c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
   document.body.className = c;
   })();
</script>
<style id='wpc-cart-css-inline-css' type='text/css'>
   .wpc_background_color { background-color : #d5d5d5 }.wpc_cart_block .wpc_background_color a.button.wc-forward { background-color : #5D78FF}a.wpc_cart_icon i { color : #fff}.wpc_cart_block .wpc_background_color a.button.wc-forward:hover { background-color : #5D78FF}.wpc_cart_block .woocommerce-mini-cart li .remove.remove_from_cart_button:hover { background-color : #5D78FF}.cart-items-count { color : #fff}
</style>
<script type='text/javascript' src='web/wp-includes/js/dist/vendor/wp-polyfill.min89b1.js?ver=7.4.4' id='wp-polyfill-js'></script>
<script type='text/javascript' id='wp-polyfill-js-after'>
   ( 'fetch' in window ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-fetch.min6e0e.js?ver=3.0.0"></scr' + 'ipt>' );( document.contains ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-node-contains.min2e00.js?ver=3.42.0"></scr' + 'ipt>' );( window.DOMRect ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-dom-rect.min2e00.js?ver=3.42.0"></scr' + 'ipt>' );( window.URL && window.URL.prototype && window.URLSearchParams ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-url.min5aed.js?ver=3.6.4"></scr' + 'ipt>' );( window.FormData && window.FormData.prototype.keys ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-formdata.mine9bd.js?ver=3.0.12"></scr' + 'ipt>' );( Element.prototype.matches && Element.prototype.closest ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-element-closest.min4c56.js?ver=2.0.2"></scr' + 'ipt>' );( 'objectFit' in document.documentElement.style ) || document.write( '<script src="web/wp-includes/js/dist/vendor/wp-polyfill-object-fit.min531b.js?ver=2.3.4"></scr' + 'ipt>' );
</script>
<script type='text/javascript' src='web/wp-includes/js/dist/hooks.minf521.js?ver=50e23bed88bcb9e6e14023e9961698c1' id='wp-hooks-js'></script>
<script type='text/javascript' src='web/wp-includes/js/dist/i18n.min87d6.js?ver=db9a9a37da262883343e941c3731bc67' id='wp-i18n-js'></script>
<script type='text/javascript' id='wp-i18n-js-after'>
   wp.i18n.setLocaleData( { 'text direction\u0004ltr': [ 'ltr' ] } );
</script>
<script type='text/javascript' src='web/wp-includes/js/dist/vendor/lodash.minf492.js?ver=4.17.19' id='lodash-js'></script>
<script type='text/javascript' id='lodash-js-after'>
   window.lodash = _.noConflict();
</script>
<script type='text/javascript' src='web/wp-includes/js/dist/url.minacd8.js?ver=0ac7e0472c46121366e7ce07244be1ac' id='wp-url-js'></script>
<script type='text/javascript' id='wp-api-fetch-js-translations'>
   ( function( domain, translations ) {
      var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
      localeData[""].domain = domain;
      wp.i18n.setLocaleData( localeData, domain );
   } )( "default", {"translation-revision-date":"2021-03-17 08:03:15+0000","generator":"GlotPress\/3.0.0-alpha.2","domain":"messages","locale_data":{"messages":{"":{"domain":"messages","plural-forms":"nplurals=2; plural=n != 1;","lang":"es"},"You are probably offline.":["Probablemente est\u00e1s desconectado."],"Media upload failed. If this is a photo or a large image, please scale it down and try again.":["La subida de medios ha fallado. Si esto es una foto o una imagen grande, por favor, reduce su tama\u00f1o e int\u00e9ntalo de nuevo."],"The response is not a valid JSON response.":["Las respuesta no es una respuesta JSON v\u00e1lida."],"An unknown error occurred.":["Ha ocurrido un error desconocido."]}},"comment":{"reference":"wp-includes\/js\/dist\/api-fetch.js"}} );
</script>
<script type='text/javascript' src='web/wp-includes/js/dist/api-fetch.minf3b9.js?ver=a783d1f442d2abefc7d6dbd156a44561' id='wp-api-fetch-js'></script>
<script type='text/javascript' id='wp-api-fetch-js-after'>
   wp.apiFetch.use( wp.apiFetch.createRootURLMiddleware( "web/wp-json/index.html" ) );
   wp.apiFetch.nonceMiddleware = wp.apiFetch.createNonceMiddleware( "f4ee650c5f" );
   wp.apiFetch.use( wp.apiFetch.nonceMiddleware );
   wp.apiFetch.use( wp.apiFetch.mediaUploadMiddleware );
   wp.apiFetch.nonceEndpoint = "web/wp-admin/admin-ajaxf809.html?action=rest-nonce";
</script>
<script type='text/javascript' id='contact-form-7-js-extra'>
   /* <![CDATA[ */
   var wpcf7 = {"cached":"1"};
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/contact-form-7/includes/js/index91d5.js?ver=5.4' id='contact-form-7-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70' id='jquery-blockui-js'></script>
<script type='text/javascript' id='wc-add-to-cart-js-extra'>
   /* <![CDATA[ */
   var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"Ver carrito","cart_url":"https:\/\/www.qfacilperu.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min0e7d.js?ver=5.1.0' id='wc-add-to-cart-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4' id='js-cookie-js'></script>
<script type='text/javascript' id='woocommerce-js-extra'>
   /* <![CDATA[ */
   var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min0e7d.js?ver=5.1.0' id='woocommerce-js'></script>
<script type='text/javascript' id='wc-cart-fragments-js-extra'>
   /* <![CDATA[ */
   var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_3bc0025c17005dfd13ef6e85d4584378","fragment_name":"wc_fragments_3bc0025c17005dfd13ef6e85d4584378","request_timeout":"5000"};
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min0e7d.js?ver=5.1.0' id='wc-cart-fragments-js'></script>
<script type='text/javascript' id='wc-cart-fragments-js-after'>
		jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
			var jetpackLazyImagesLoadEvent;
			try {
				jetpackLazyImagesLoadEvent = new Event( 'jetpack-lazy-images-load', {
					bubbles: true,
					cancelable: true
				} );
			} catch ( e ) {
				jetpackLazyImagesLoadEvent = document.createEvent( 'Event' )
				jetpackLazyImagesLoadEvent.initEvent( 'jetpack-lazy-images-load', true, true );
			}
			jQuery( 'body' ).get( 0 ).dispatchEvent( jetpackLazyImagesLoadEvent );
		} );
		
</script>
<script type='text/javascript' id='mailchimp-woocommerce-js-extra'>
   /* <![CDATA[ */
   var mailchimp_public_data = {"site_url":"https:\/\/www.qfacilperu.com","ajax_url":"https:\/\/www.qfacilperu.com\/wp-admin\/admin-ajax.php","language":"es"};
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/mailchimp-for-woocommerce/public/js/mailchimp-woocommerce-public.min9776.js?ver=2.5.1' id='mailchimp-woocommerce-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/moment.mine7f0.js?ver=1.3.1' id='wpc-moment-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/flatpickr.mine7f0.js?ver=1.3.1' id='wpc-flatpicker-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/jquery.timepicker.mine7f0.js?ver=1.3.1' id='wpc-jquery-timepicker-js'></script>
<script type='text/javascript' id='wpc-public-js-extra'>
   /* <![CDATA[ */
   var wpc_form_client_data = "{\"settings\":[],\"wpc_ajax_url\":\"https:\\\/\\\/www.qfacilperu.com\\\/wp-admin\\\/admin-ajax.php\"}";
   /* ]]> */
</script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/wpc-publice7f0.js?ver=1.3.1' id='wpc-public-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/commone7f0.js?ver=1.3.1' id='wpc-common-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/TweenMax.min7406.js?ver=2.0.1' id='tweenmax-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/jquery.easing.1.37406.js?ver=2.0.1' id='jquery-easing-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/tilt.jquery.min7406.js?ver=2.0.1' id='tilt-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/anime7406.js?ver=2.0.1' id='animejs-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/magician7406.js?ver=2.0.1' id='magicianjs-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementskit-lite/libs/framework/assets/js/frontend-script77e6.js?ver=2.2.1' id='elementskit-framework-js-frontend-js'></script>
<script type='text/javascript' id='elementskit-framework-js-frontend-js-after'>
   var elementskit = {
         resturl: 'https://www.qfacilperu.com/wp-json/elementskit/v1/',
      }
</script>
<script type='text/javascript' src='web/wp-content/plugins/elementskit-lite/widgets/init/assets/js/widget-scripts77e6.js?ver=2.2.1' id='ekit-widget-scripts-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/bootstrap.min7406.js?ver=2.0.1' id='bootstrap-min-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/popper.min7406.js?ver=2.0.1' id='popper-min-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/jquery.magnific-popup.min7406.js?ver=2.0.1' id='jquery-magnific-popup-min-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/instafeed.min7406.js?ver=2.0.1' id='instafeed-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/owl.carousel.min7406.js?ver=2.0.1' id='owl-carousel-min-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/jquery.easypiechart.min7406.js?ver=2.0.1' id='jquery-easypiechart-min-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/script7406.js?ver=2.0.1' id='gloreya-script-js'></script>
<script type='text/javascript' src='web/wp-includes/js/wp-embed.mine23c.js?ver=5.7' id='wp-embed-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/js/webpack.runtime.minaeb9.js?ver=3.1.4' id='elementor-webpack-runtime-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/js/frontend-modules.minaeb9.js?ver=3.1.4' id='elementor-frontend-modules-js'></script>
<script type='text/javascript' src='web/wp-includes/js/jquery/ui/core.min35d0.js?ver=1.12.1' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/lib/dialog/dialog.mina288.js?ver=4.8.1' id='elementor-dialog-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min05da.js?ver=4.0.2' id='elementor-waypoints-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/lib/share-link/share-link.minaeb9.js?ver=3.1.4' id='share-link-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/lib/swiper/swiper.min48f5.js?ver=5.3.6' id='swiper-js'></script>
<script type='text/javascript' id='elementor-frontend-js-before'>
   var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false,"isImprovedAssetsLoading":false},"i18n":{"shareOnFacebook":"Compartir en Facebook","shareOnTwitter":"Compartir en Twitter","pinIt":"Pinear","download":"Descargar","downloadImage":"Descargar imagen","fullscreen":"Pantalla completa","zoom":"Zoom","share":"Compartir","playVideo":"Reproducir v\u00eddeo","previous":"Anterior","next":"Siguiente","close":"Cerrar"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.1.4","is_static":false,"experimentalFeatures":[],"urls":{"assets":"https:\/\/www.qfacilperu.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":579,"title":"Menu%20%E2%80%93%20QFacil%20Per%C3%BA","excerpt":"","featuredImage":false}};
</script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/js/frontend.minaeb9.js?ver=3.1.4' id='elementor-frontend-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/wp-cafe/assets/js/elementore7f0.js?ver=1.3.1' id='wpc-elementor-frontend-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/assets/js/elementor7406.js?ver=2.0.1' id='gloreya-main-elementor-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementskit-lite/widgets/init/assets/js/slick.min77e6.js?ver=2.2.1' id='ekit-slick-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementskit-lite/widgets/init/assets/js/elementor77e6.js?ver=2.2.1' id='elementskit-elementor-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/widget-init7406.js?ver=2.0.1' id='elementskit-parallax-widget-init-js'></script>
<script type='text/javascript' src='web/wp-content/themes/gloreya/core/parallax/assets/js/section-init7406.js?ver=2.0.1' id='elementskit-parallax-section-init-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementor/assets/js/preloaded-elements-handlers.minaeb9.js?ver=3.1.4' id='preloaded-elements-handlers-js'></script>
<script type='text/javascript' src='web/wp-content/plugins/elementskit-lite/modules/controls/assets/js/widgetarea-editor77e6.js?ver=2.2.1' id='elementskit-js-widgetarea-control-editor-js'></script>
<script type='text/javascript'>

    function verProductos(id) {

        $.post( "{{ Route('mostrarProductos') }}", {id: id, _token:'{{csrf_token()}}'}).done(function(data) {
            
        });

        

    }

</script>
</body>
</html>
