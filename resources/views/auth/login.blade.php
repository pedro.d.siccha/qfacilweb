<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Core Login Form Flat Responsive Widget Template :: w3layouts</title>
    <!-- Meta-Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="Core Login Form a Responsive Web Template, Bootstrap Web Templates, Flat Web Templates, Android Compatible Web Template, Smartphone Compatible Web Template, Free Webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design">
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta-Tags -->
    <!-- Index-Page-CSS -->
    <link rel="stylesheet" href="{{ ('lg/css/style.css') }}" type="text/css" media="all">
    <!-- //Custom-Stylesheet-Links -->
    <!--fonts -->
    <link href="//fonts.googleapis.com/css?family=Mukta+Mahee:200,300,400,500,600,700,800" rel="stylesheet">
    <!-- //fonts -->
    <!-- Font-Awesome-File -->
    <link rel="stylesheet" href="{{ ('lg/css/font-awesome.css') }}" type="text/css" media="all">
</head>

<body>
    <h1 class="title-agile text-center"></h1>
    <div class="content-w3ls">
        <div class="agileits-grid">
            <h1 class="title-agile text-center"></h1>
            <div class="content-bottom">
                @if (session('status'))
                    <div class="mb-4 font-medium text-sm text-green-600">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="field_w3ls">
                        <div class="field-group">
                            <input name="email" :value="old('email')" id="email" type="email" value="" placeholder="Correo de Usuario" required autofocus>
                        </div>
                        <div class="field-group">
                            <input id="password" type="password" class="form-control" name="password" value="" placeholder="Contraseña" required autocomplete="current-password">
                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                        </div>
                    </div>
                    <div class="wthree-field">
                        <input id="saveForm" name="saveForm" type="submit" value="EMPEZAR" />
                    </div>
                    <ul class="list-login">
                        <li class="switch-agileits">
                            <label class="switch">
                                <input type="checkbox">
                                <span class="slider round"></span>
                                keep me signed in
                            </label>
                        </li>
                        <li>
                            @if (Route::has('password.request'))
                            <a class="text-right underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                                OLVIDÉ MI CONTRASEÑA
                            </a>
                        @endif
                        </li>
                        <li class="clearfix"></li>
                    </ul>
                </form>
            </div>
            <!-- //content bottom -->
        </div>
    </div>
    <div class="copyright text-center">
        <p>© 2018 Core Login Form. All rights reserved | Design by
            <a href="http://w3layouts.com">W3layouts</a>
        </p>
    </div>
    <!--//copyright-->
    <script src="{{ ('lg/js/jquery-2.2.3.min.js') }}"></script>
    <!-- script for show password -->
    <script>
        $(".toggle-password").click(function () {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>
    <!-- /script for show password -->

</body>
<!-- //Body -->

</html>