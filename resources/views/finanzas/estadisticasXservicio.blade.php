@extends('layouts.app')
@section('titulo')
    EST. DE SERVICIOS
@endsection

@section('contenido')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>ESTADISTICAS DE SERVICIOS</h2>
    
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ Route('inicio') }}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Finanzas</span></li>
                <li><span>Estadistica de Servicios</span></li>
            </ol>
    
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
</section>
@endsection