@extends('layouts.app')
@section('titulo')
    GESTIÓN DE CAPITAL
@endsection

@section('contenido')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>GESTIÓN DE CAPITAL</h2>
    
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ Route('inicio') }}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Administración</span></li>
                <li><span>Gestión de Capital</span></li>
            </ol>
    
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
    <div class="col-md-12 col-lg-12 col-xl-4">
        <h5 class="text-semibold text-dark text-uppercase mb-md mt-lg">Capital</h5>
        <div class="row">
            <div class="col-md-6 col-xl-12">
                <section class="panel">
                    <div class="panel-body bg-primary">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon">
                                    <i class="fa fa-life-ring"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Caja Chica</h4>
                                    <div class="info">
                                        <strong class="amount">S/. 0.00</strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-uppercase">(Administración)</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-6 col-xl-12">
                <section class="panel">
                    <div class="panel-body bg-secondary">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon">
                                    <i class="fa fa-life-ring"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Bancos</h4>
                                    <div class="info">
                                        <strong class="amount">S/. 0.00</strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-uppercase">(Administración)</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Historial de Movimientos</h2>
            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-none">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Fecha</th>
                                <th>Hora</th>
                                <th>Monto Inicial</th>
                                <th>Monto Final</th>
                                <th>Tipo de Caja</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</section>
@endsection