@extends('layouts.app')
@section('titulo')
    GASTOS ADMINISTRATIVOS
@endsection

@section('contenido')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>GASTOS ADMINISTRATIVOS</h2>
    
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ Route('inicio') }}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Administración</span></li>
                <li><span>Gastos Administrativos</span></li>
            </ol>
    
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <div class="row">
        <div class="col-md-12">
            <div class="tabs">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#popular" data-toggle="tab"><i class="fa fa-star"></i> Caja Chica</a>
                    </li>
                    <li>
                        <a href="#recent" data-toggle="tab">Bancos</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="popular" class="tab-pane active">
                        
                        <div class="row">
							<div class="col-md-12 col-lg-12 col-xl-3">
								<section class="panel panel-featured-left panel-featured-primary">
									<div class="panel-body">
										<div class="widget-summary">
											<div class="widget-summary-col widget-summary-col-icon">
												<div class="summary-icon bg-primary">
													<i class="fa fa-life-ring"></i>
												</div>
											</div>
											<div class="widget-summary-col">
												<div class="summary">
													<h4 class="title">Caja Chica</h4>
													<div class="info">
														<strong class="amount">S/. 0.00</strong>
														<span class="text-primary"><?php date_default_timezone_set('America/Lima'); echo date('d-m-Y h:i:s a', time()); ?></span>
													</div>
												</div>
												<div class="summary-footer">
													<a class="text-muted text-uppercase">(actualizar)</a>
												</div>
											</div>
										</div>
									</div>
								</section>
							</div>
						</div>
                        
                    </div>
                    <div id="recent" class="tab-pane">
                        <p>Recent</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection