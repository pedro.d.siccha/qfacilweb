<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PersonalshoperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function perfilShoper()
    {
        $auth = \Auth::user();

        $user = \DB::SELECT('SELECT * FROM persona p, users u
                            WHERE p.users_id = u.id AND p.users_id = "'.$auth->id.'"
                         LIMIT 1');

        return view('personalShoper.perfilShoper', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historialShoper()
    {
        $auth = \Auth::user();

        $user = \DB::SELECT('SELECT * FROM persona p, users u
                            WHERE p.users_id = u.id AND p.users_id = "'.$auth->id.'"
                         LIMIT 1');

        return view('personalShoper.historialShoper', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function soporteTecnicoShoper()
    {
        $auth = \Auth::user();

        $user = \DB::SELECT('SELECT * FROM persona p, users u
                            WHERE p.users_id = u.id AND p.users_id = "'.$auth->id.'"
                         LIMIT 1');

        return view('personalShoper.soporteTecnicoShoper', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sugerenciasShoper()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logrosShoper()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
