<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PickerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function perfilPicker()
    {
        $auth = \Auth::user();

        $user = \DB::SELECT('SELECT * FROM persona p, users u
                            WHERE p.users_id = u.id AND p.users_id = "'.$auth->id.'"
                         LIMIT 1');

        return view('picker.perfilPicker', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historial()
    {
        $auth = \Auth::user();

        $user = \DB::SELECT('SELECT * FROM persona p, users u
                            WHERE p.users_id = u.id AND p.users_id = "'.$auth->id.'"
                         LIMIT 1');

        return view('picker.historial', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sugerencias()
    {
        $auth = \Auth::user();

        $user = \DB::SELECT('SELECT * FROM persona p, users u
                            WHERE p.users_id = u.id AND p.users_id = "'.$auth->id.'"
                         LIMIT 1');

        return view('picker.sugerencias', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function soporteTecnico()
    {
        $auth = \Auth::user();

        $user = \DB::SELECT('SELECT * FROM persona p, users u
                            WHERE p.users_id = u.id AND p.users_id = "'.$auth->id.'"
                         LIMIT 1');

        return view('picker.soporteTecnico', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logros()
    {
        $auth = \Auth::user();

        $user = \DB::SELECT('SELECT * FROM persona p, users u
                            WHERE p.users_id = u.id AND p.users_id = "'.$auth->id.'"
                         LIMIT 1');

        return view('picker.logros', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
