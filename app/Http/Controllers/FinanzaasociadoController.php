<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FinanzaasociadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function misGananciasAsociado()
    {
        $auth = \Auth::user();

        $user = \DB::SELECT('SELECT * FROM persona p, users u
                            WHERE p.users_id = u.id AND p.users_id = "'.$auth->id.'"
                         LIMIT 1');

        return view('finanzasAsociado.ganancias', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function misMetasAsociado()
    {
        $auth = \Auth::user();

        $user = \DB::SELECT('SELECT * FROM persona p, users u
                            WHERE p.users_id = u.id AND p.users_id = "'.$auth->id.'"
                         LIMIT 1');

        return view('finanzasAsociado.metas', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
