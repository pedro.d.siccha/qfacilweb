<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function paginaWebAdmin()
    {
        return view('web.paginaWebAdmin');
    }

    public function nosotrosAdmin()
    {
        return view('web.nosotrosAdmin');
    }

    public function serviciosAdmin()
    {
        return view('web.serviciosAdmin');
    }

    public function preguntaFrecuenteAdmin()
    {
        return view('web.preguntaFrecuenteAdmin');
    }

    public function blogAdmin()
    {
        return view('web.blogAdmin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function nosotros()
    {
        $banner = \DB::SELECT('SELECT * FROM banner');

        $tienda = \DB::SELECT('SELECT t.nombre AS nombre, ct.categoria AS categoria, t.foto 
                           FROM tienda t, categoria_tienda ct
                           WHERE t.categoria_tienda_id = ct.id');

        $equipo = \DB::SELECT('SELECT tu.tipo tipousuario, p.nombre AS nombre, p.apellido AS apellido, p.foto
                               FROM users u, tipo_usuario tu, persona p
                               WHERE tu.id = u.id AND p.users_id = u.id AND tu.tipo != "CLIENTE"');

        $comentario = \DB::SELECT('SELECT c.titulo AS titulo, c.comentario AS comentario, u.name AS usuario, p.foto
                                   FROM comentario c, users u, persona p
                                   WHERE c.users_id = u.id AND p.users_id = u.id
                                   LIMIT 5');

        return view('web.nosotros', compact('banner', 'tienda', 'equipo', 'comentario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function blog()
    {

        $banner = \DB::SELECT('SELECT * FROM banner');

        $tienda = \DB::SELECT('SELECT t.nombre AS nombre, ct.categoria AS categoria, t.foto 
                           FROM tienda t, categoria_tienda ct
                           WHERE t.categoria_tienda_id = ct.id');

        return view('web.blog', compact('banner', 'tienda'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function preguntas()
    {

        $banner = \DB::SELECT('SELECT * FROM banner');

        $tienda = \DB::SELECT('SELECT t.nombre AS nombre, ct.categoria AS categoria, t.foto 
                           FROM tienda t, categoria_tienda ct
                           WHERE t.categoria_tienda_id = ct.id');

        return view('web.preguntas', compact('banner', 'tienda'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
