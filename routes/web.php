<?php

use App\Http\Controllers\AdministracionasociadoController;
use App\Http\Controllers\AdministracionController;
use App\Http\Controllers\AnuncioasociadoController;
use App\Http\Controllers\AtencionclienteController;
use App\Http\Controllers\ConfiguracionasociadoController;
use App\Http\Controllers\ExtraController;
use App\Http\Controllers\FinanzaasociadoController;
use App\Http\Controllers\FinanzasController;
use App\Http\Controllers\MarketingController;
use App\Http\Controllers\MensajeController;
use App\Http\Controllers\PersonalshoperController;
use App\Http\Controllers\PickerController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\RecursoshumanosController;
use App\Http\Controllers\TiendasController;
use App\Http\Controllers\WebController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $banner = \DB::SELECT('SELECT * 
                           FROM banner');

    $servicios = \DB::SELECT('SELECT * 
                              FROM servicios WHERE estado = "ACTIVO"');

    $categoria = \DB::SELECT('SELECT * 
                              FROM categoria_tienda');

    $producto = \DB::SELECT('SELECT * 
                             FROM producto');

    $imagen = \DB::SELECT('SELECT * 
                           FROM producto p, producto_imagen pig, imagen i
                           WHERE pig.producto_id = p.id AND pig.imagen_id = i.id
                           LIMIT 1');

    $comentario = \DB::SELECT('SELECT c.titulo AS titulo, c.comentario AS comentario, u.name AS usuario, p.foto
                               FROM comentario c, users u, persona p
                               WHERE c.users_id = u.id AND p.users_id = u.id
                               LIMIT 5');

    $persona = \DB::SELECT('SELECT * FROM persona');

    $tienda = \DB::SELECT('SELECT t.nombre AS nombre, ct.categoria AS categoria, t.foto 
                           FROM tienda t, categoria_tienda ct
                           WHERE t.categoria_tienda_id = ct.id');

    //return view('welcome', compact('banner', 'servicios', 'categoria', 'producto', 'imagen', 'comentario'));
    return view('web.inicio', compact('banner', 'servicios', 'categoria', 'producto', 'imagen', 'comentario', 'persona', 'tienda'));
})->name('home');

Route::get('/nosotros', [WebController::class, 'nosotros'])->name('nosotros');

Route::get('/nosotrosUser', [WebController::class, 'nosotrosUser'])->name('nosotrosUser');

Route::get('/blogUser', [WebController::class, 'blogUser'])->name('blogUser');

Route::get('/blog', [WebController::class, 'blog'])->name('blog');

Route::get('/preguntasUser', [WebController::class, 'preguntasUser'])->name('preguntasUser');

Route::get('/preguntas', [WebController::class, 'preguntas'])->name('preguntas');

Route::post('/mostrarProductos', function (Request $request) {
    dd($request);
})->name('mostrarProductos');

Route::middleware(['auth:sanctum', 'verified'])->get('/inicio', function () {

    $auth = \Auth::user();

    $user = \DB::SELECT('SELECT * FROM persona p, users u
                         WHERE p.users_id = u.id AND p.users_id = "'.$auth->id.'"
                         LIMIT 1');
    
    return view('dashboard', compact('user'));
})->name('inicio');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {

    $auth = \Auth::user();

    $user = \DB::SELECT('SELECT * FROM persona
                         WHERE users_id = "'.$auth->id.'"
                         LIMIT 1');

    $banner = \DB::SELECT('SELECT * 
                           FROM banner');

    $servicios = \DB::SELECT('SELECT * 
                              FROM servicios WHERE estado = "ACTIVO"');

    $categoria = \DB::SELECT('SELECT * 
                              FROM categoria_tienda');

    $producto = \DB::SELECT('SELECT * 
                             FROM producto');

    $imagen = \DB::SELECT('SELECT * 
                           FROM producto p, producto_imagen pig, imagen i
                           WHERE pig.producto_id = p.id AND pig.imagen_id = i.id
                           LIMIT 1');

    $comentario = \DB::SELECT('SELECT c.titulo AS titulo, c.comentario AS comentario, u.name AS usuario, p.foto
                               FROM comentario c, users u, persona p
                               WHERE c.users_id = u.id AND p.users_id = u.id
                               LIMIT 5');

    $persona = \DB::SELECT('SELECT * FROM persona');

    $tienda = \DB::SELECT('SELECT t.nombre AS nombre, ct.categoria AS categoria, t.foto 
                           FROM tienda t, categoria_tienda ct
                           WHERE t.categoria_tienda_id = ct.id');

    $permiso = \DB::SELECT('SELECT * 
                            FROM users u, tipo_usuario tu
                            WHERE u.tipo_usuario_id = tu.id AND u.id = "'.$auth->id.'"');

    return view('web.inicioUser', compact('banner', 'servicios', 'categoria', 'producto', 'imagen', 'comentario', 'persona', 'tienda', 'user', 'permiso'));
})->name('dashboard');

Route::get('/panelControl', [WebController::class, 'panelControl'])->name('panelControl');

//Atencion al Cliente
Route::get('/lista', [AtencionclienteController::class, 'lista'])->name('lista');
Route::get('/sugerencia', [AtencionclienteController::class, 'sugerencia'])->name('sugerencia');

//Recursos Humanos
Route::get('/personalAdministrativo', [RecursoshumanosController::class, 'personalAdministrativo'])->name('personalAdministrativo');
Route::get('/personalCampo', [RecursoshumanosController::class, 'personalCampo'])->name('personalCampo');
Route::get('/pagoPersonal', [RecursoshumanosController::class, 'pagoPersonal'])->name('pagoPersonal');
Route::get('/seguridad', [RecursoshumanosController::class, 'seguridad'])->name('seguridad');
Route::get('/contrato', [RecursoshumanosController::class, 'contrato'])->name('contrato');

//Administracion
Route::get('/gestionCapital', [AdministracionController::class, 'gestionCapital'])->name('gestionCapital');
Route::get('/gastosAdministrativos', [AdministracionController::class, 'gastosAdministrativos'])->name('gastosAdministrativos');
Route::get('/configuraciones', [AdministracionController::class, 'configuraciones'])->name('configuraciones');

//Finanzas
Route::get('/estadisticasXasociado', [FinanzasController::class, 'estadisticasXasociado'])->name('estadisticasXasociado');
Route::get('/estadisticasXservicio', [FinanzasController::class, 'estadisticasXservicio'])->name('estadisticasXservicio');
Route::get('/analisisResultado', [FinanzasController::class, 'analisisResultado'])->name('analisisResultado');
Route::get('/flujoCaja', [FinanzasController::class, 'flujoCaja'])->name('flujoCaja');
Route::get('/controlPatrimonio', [FinanzasController::class, 'controlPatrimonio'])->name('controlPatrimonio');

//Marketing
Route::get('/clientesPotenciales', [MarketingController::class, 'clientesPotenciales'])->name('clientesPotenciales');
Route::get('/controlGastos', [MarketingController::class, 'controlGastos'])->name('controlGastos');
Route::get('/reportes', [MarketingController::class, 'reportes'])->name('reportes');
Route::get('/gestionAnuncios', [MarketingController::class, 'gestionAnuncios'])->name('gestionAnuncios');

//Pagina Web
Route::get('/paginaWebAdmin', [WebController::class, 'paginaWebAdmin'])->name('paginaWebAdmin');
Route::get('/nosotrosAdmin', [WebController::class, 'nosotrosAdmin'])->name('nosotrosAdmin');
Route::get('/serviciosAdmin', [WebController::class, 'serviciosAdmin'])->name('serviciosAdmin');
Route::get('/preguntaFrecuenteAdmin', [WebController::class, 'preguntaFrecuenteAdmin'])->name('preguntaFrecuenteAdmin');
Route::get('/blogAdmin', [WebController::class, 'blogAdmin'])->name('blogAdmin');

//Mensajes
Route::get('/inbox', [MensajeController::class, 'inbox'])->name('inbox');
Route::get('/leidos', [MensajeController::class, 'leidos'])->name('leidos');
Route::get('/enviado', [MensajeController::class, 'enviado'])->name('enviado');

//Picker
Route::get('/perfilPicker', [PickerController::class, 'perfilPicker'])->name('perfilPicker');
Route::get('/historialPiker', [PickerController::class, 'historial'])->name('historialPiker');
Route::get('/sugerenciasPiker', [PickerController::class, 'sugerencias'])->name('sugerenciasPiker');
Route::get('/soporteTecnicoPiker', [PickerController::class, 'soporteTecnico'])->name('soporteTecnicoPiker');
Route::get('/logrosPiker', [PickerController::class, 'logros'])->name('logrosPiker');

//Personal Shoper
Route::get('/perfilShoper', [PersonalshoperController::class, 'perfilShoper'])->name('perfilShoper');
Route::get('/historialShoper', [PersonalshoperController::class, 'historialShoper'])->name('historialShoper');
Route::get('/soporteTecnicoShoper', [PersonalshoperController::class, 'soporteTecnicoShoper'])->name('soporteTecnicoShoper');
Route::get('/sugerenciasShoper', [PersonalshoperController::class, 'sugerenciasShoper'])->name('sugerenciasShoper');
Route::get('/logrosShoper', [PersonalshoperController::class, 'logrosShoper'])->name('logrosShoper');

//Tiendas
Route::get('/tiendas', [TiendasController::class, 'tiendas'])->name('tiendas');
Route::get('/buzonAsociado', [TiendasController::class, 'buzonAsociado'])->name('buzonAsociado');
Route::get('/enviarAsociado', [TiendasController::class, 'enviarAsociado'])->name('enviarAsociado');

//Administracion Asociado
Route::get('/perfilAsociado', [AdministracionasociadoController::class, 'perfilAsociado'])->name('perfilAsociado');

//Anuncios Asociado
Route::get('/verAnuncioAsociado', [AnuncioasociadoController::class, 'verAnuncioAsociado'])->name('verAnuncioAsociado');
Route::get('/logrosAnuncioAsociado', [AnuncioasociadoController::class, 'logrosAnuncioAsociado'])->name('logrosAnuncioAsociado');

//Configuraciones Asociado
Route::get('/basicoAsociado', [ConfiguracionasociadoController::class, 'basicoAsociado'])->name('basicoAsociado');
Route::get('/generalAsociado', [ConfiguracionasociadoController::class, 'generalAsociado'])->name('generalAsociado');

//Finanzas Asociado
Route::get('/misGananciasAsociado', [FinanzaasociadoController::class, 'misGananciasAsociado'])->name('misGananciasAsociado');
Route::get('/misMetasAsociado', [FinanzaasociadoController::class, 'misMetasAsociado'])->name('misMetasAsociado');

//Extras
Route::get('/miPerfil', [ExtraController::class, 'miPerfil'])->name('miPerfil');